﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
    public class AddressInformationViewModel
    {
        public AddressViewModel AddressViewModel { get; set; }
        public List<AddressTypeViewModel> AddressTypeViewModels { get; set; }
    }
}