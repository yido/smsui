﻿using System;

namespace SMSUI.ViewModels
{
    public class DemographicInfoViewModel
    {
        public int DemographicInfoId { get; set; }
        public string Sex { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Nationality { get; set; }
        public int Age { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Weight { get; set; }
        public string BloodGroup { get; set; }
        public string Religion { get; set; }  
    }
}