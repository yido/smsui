﻿using SMSUI.ViewModels.Staff;

namespace SMSUI.ViewModels.Exam
{
    public class TeacherBatchExamViewModel
    {
        public int TeacherBatchExamID { get; set; }
        public TeacherBatchViewModel TeacherBatch { get; set; }
        public ExamViewModel Exam { get; set; }
        public bool IsOwner { get; set; }
    }
}