﻿namespace SMSUI.ViewModels.Exam
{
    public class ExamViewModel
    {
        public int ExamID { get; set; }
        public string Name { get; set; }
        public double Mark { get; set; }

        public string ExamFullName
        {
            get { return Name + " ("+ Mark+"%)"; }
        }

        public ExamTypeViewModel  ExamType { get; set; }
        
        //TODO We need ExamDate too plus some description column (optional)
    }
}