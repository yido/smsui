﻿using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Exam
{
    public class ExamSchedulePViewModel
    {
        public IList<GradeLevelViewModel> GradeLevelViewModels { get; set; }
        public IList<StandardExamViewModel> StandardExamViewModels { get; set; }
        public GradeLevelViewModel SelectedGradeLevelViewModel { get; set; }
        public IList<ExamTypeViewModel> ExamTypeViewModels { get; set; }

    }
}