﻿namespace SMSUI.ViewModels.Exam
{
    public class ExamTypeViewModel
    {
        public int ExamTypeID { get; set; }
        public string Name { get; set; }
        public ParentExamTypeViewModel ParentExamType { get; set; }

    }
}