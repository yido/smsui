﻿using System;
using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Exam
{
    public class StandardExamViewModel
    {
        public int ExamID { get; set; }
        public string Name { get; set; }
        public ExamTypeViewModel ExamTypeViewModel { get; set; }
        public int Marks { get; set; }
        public DateTime ExamDate { get; set; }
        public IList<SubjectViewModel> SubjectViewModels { get; set; }


    }
}