﻿using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Exam
{
    public class ExamInfoViewModel
    {
        public IList<ExamTypeViewModel> ExamTypes { get; set; }
        public IList<ClassViewModel> Classes { get; set; }
        public IList<SubjectViewModel> Subjects { get; set; }
        public IList<TeacherBatchExamViewModel> TeacherBatchExams { get; set; }

    }
}