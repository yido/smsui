﻿namespace SMSUI.ViewModels.Exam
{
    public class ParentExamTypeViewModel
    {
        public int ParentExamTypeID { get; set; }
        public string Name  { get; set; }
    }
}