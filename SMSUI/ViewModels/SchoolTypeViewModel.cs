﻿namespace SMSUI.ViewModels
{
    public class SchoolTypeViewModel
    {
        public int  SchoolTypeId { get; set; }
        public string  Name { get; set; }
    }
}