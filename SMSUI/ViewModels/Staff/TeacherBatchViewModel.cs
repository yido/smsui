﻿using SMSUI.ViewModels.SchoolSetting;
﻿using System.Collections.Generic;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.ViewModels.Staff
{
    public class TeacherBatchViewModel
    {
        public TeacherBatchViewModel()
        {
            TeacherTerm = new TeacherTermViewModel();
            Subject = new SubjectViewModel();
            TeacherBatchExamViewModels = new List<TeacherBatchExamViewModel>();
        }
        public int TeacherBatchID { get; set; }
        public TeacherTermViewModel TeacherTerm { get; set; }
        public SubjectViewModel Subject { get; set; }

        public GradeLevelViewModel GradeLevelViewModel
        {
            get {
                return TeacherTerm.Class.GradeLevelViewModel;
            }
        }
        public List<TeacherBatchExamViewModel> TeacherBatchExamViewModels { get; set; }

        public string ClassNameWithSubject { get { return TeacherTerm.Class.Name + " - " + Subject; } }
    }

}