﻿using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Staff
{
    public class TeacherBatchPageViewModel
    {
        public IList<TeacherBatchViewModel> TeacherBatchViewModels { get; set; }

        public IList<SubjectViewModel> SubjectViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(a => a.Subject).ToList();
            }
        }

        public IList<GradeLevelViewModel> GradeLevelViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(a => a.GradeLevelViewModel).ToList();
            }
        }

        public IList<ClassViewModel> ClassViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(a => a.TeacherTerm.Class).ToList();
            }
        }

        public bool IsSupervisor { get; set; }

    }
}