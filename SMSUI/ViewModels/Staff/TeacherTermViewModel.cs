﻿using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Staff
{
    public class TeacherTermViewModel
    {
        public TeacherTermViewModel() { Class = new ClassViewModel(); }
        public int TeacherTermID { get; set; }
        public TeacherViewModel Teacher { get; set; }
        public ClassViewModel Class { get; set; }
        public TermViewModel Term { get; set; }
        public bool IsSupervisor { get; set; }
    }
}