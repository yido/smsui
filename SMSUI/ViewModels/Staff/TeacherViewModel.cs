﻿using System.ComponentModel;

namespace SMSUI.ViewModels.Staff
{
    public class TeacherViewModel
    {
        public int TeacherID { get; set; }

        [DisplayName("ID #")]
        public string IdNumber { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Middle Name")]
        public string MiddleName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }
       
        public bool IsActive { get; set; }
        public string Photo { get; set; }
        public AddressViewModel Address { get; set; }
        public DemographicInfoViewModel DemographicInformation { get; set; }

        public string FullName
        {
            get
            {
                var title = DemographicInformation.Sex == "Male" ? "Mr." : "Mrs.";
                return title + " " + FirstName + " " + MiddleName;
            }
        }
    }
}