﻿using System;

namespace SMSUI.ViewModels.Setting
{
    public class PeriodSettingViewModel
    {
        public int WorkingHour { get; set; }
        
        public int PeriodDuration { get; set; }
        public TimeSpan ClassStartTime { get; set; }
        public int MorningBreakDuration { get; set; }
        public TimeSpan MorningBreakStartTime { get; set; }
        public int LunchBreakDuration { get; set; }
        public TimeSpan LunchBreakStartTime { get; set; }

        public TimeSpan ClassEndTime { get; set; }
        
        public int NumberOfPeriod
        {
            get
            {
                var workinghr = (WorkingHour - MorningBreakDuration - LunchBreakDuration);//assuming all are in min or hr
                return (workinghr/PeriodDuration);
            }
        }


    }
}