﻿using System;

namespace SMSUI.ViewModels.Setting
{
    public class PeriodViewModel
    {
        public int PeriodID { get; set; }
        public string Name { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; } //This will b calculated field

        public bool ValidateDay()
        {
            return (EndTime - StartTime).Days == 0;
        }

        
    }
}