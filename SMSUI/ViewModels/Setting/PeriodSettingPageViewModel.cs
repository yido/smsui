﻿using System.Collections;
using System.Collections.Generic;

namespace SMSUI.ViewModels.Setting
{
    public class PeriodSettingPageViewModel
    {
        public PeriodSettingViewModel PeriodSettingViewModel { get; set; }
        public IList<PeriodViewModel> PeriodViewModels { get; set; }
    }
}