﻿namespace SMSUI.ViewModels
{
    public class StudentsForRegistrationViewModel
    {
        public int StudentId { get; set; }
        public string IdNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public string StudentType { get; set; }
        public string StudentLevelType { get; set; }
        public string RegistrationStatus { get; set; }

        public int StudentTypeId { get; set; }
        public int StudentLevelTypeId { get; set; }
        public int RegistrationStatusId { get; set; }
    }
}