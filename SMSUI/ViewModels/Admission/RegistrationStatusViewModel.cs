﻿namespace SMSUI.ViewModels
{
    public class RegistrationStatusViewModel
    {
        public int RegistrationStatusId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}