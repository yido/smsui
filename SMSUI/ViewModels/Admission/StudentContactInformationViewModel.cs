﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
    public class StudentContactInformationViewModel
    {
        public StudentContactInformationViewModel()
        {
            DemographicInfoViewModel= new DemographicInfoViewModel();
            ContactPersonViewModels = new List<ContactPersonViewModel>();
        }
        public DemographicInfoViewModel DemographicInfoViewModel { get; set; }
        public List<ContactPersonViewModel> ContactPersonViewModels { get; set; }
    }
}