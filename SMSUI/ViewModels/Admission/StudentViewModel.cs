﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
    public class StudentViewModel
    {
        public StudentViewModel()
        {
            AddressViewModel = new AddressViewModel(); 
            StudentLevelType = new StudentLevelTypeViewModel();
            StudentType = new StudentTypeViewModel();
            AddressViewModel = new AddressViewModel();
            DemographicInfo = new DemographicInfoViewModel();
            RegistrationStatus = new RegistrationStatusViewModel();
            ContactPersonViewModels = new List<ContactPersonViewModel>();

        }
        public int StudentId { get; set; }
        public string IdNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PhotoPath { get; set; }
        public bool IsActive { get; set; }

        public int StudentTypeId { get { return StudentType.StudentTypeId; } }
        public string StudentTypeName { get { return StudentType.Name; } }
        public int StudentLevelTypeId { get { return StudentLevelType.StudentLevelTypeId; } }
        public string StudentLevelTypeName { get { return StudentLevelType.Name; } }
        public int AddressId { get { return AddressViewModel.AddressId; } }
        public int DemographicInfoId { get { return DemographicInfo.DemographicInfoId; } }
        public int RegistrationStatusId { get { return RegistrationStatus.RegistrationStatusId; } }
        public StudentLevelTypeViewModel StudentLevelType { get; set; }
        public StudentTypeViewModel StudentType { get; set; }
        public AddressViewModel AddressViewModel { get; set; }
        public DemographicInfoViewModel DemographicInfo { get; set; }
        public RegistrationStatusViewModel RegistrationStatus { get; set; }
        public IList<ContactPersonViewModel> ContactPersonViewModels { get; set; }

        public string StudentFullName
        {
            get
            {
                return FirstName + " " + MiddleName + " " + LastName;
            }
        }
    }
}