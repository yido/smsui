﻿namespace SMSUI.ViewModels
{
    public class AdmissionViewModel
    {
        public int StudentId
        {
            get { return StudentInformationViewModel.StudentViewModel.AddressId; }
        }

        public StudentInformationViewModel StudentInformationViewModel { get; set; }
        public StudentContactInformationViewModel StudentContactInformationViewModel { get; set; }
        public StudentIdCardInformationViewModel StudentIdCardInformationViewModel { get; set; }

    }
}