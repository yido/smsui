﻿namespace SMSUI.ViewModels
{
    public class StudentLevelTypeViewModel
    {
        public int StudentLevelTypeId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}