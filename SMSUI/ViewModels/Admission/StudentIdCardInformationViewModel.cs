﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
    public class StudentIdCardInformationViewModel
    {
        public IdCardViewModel IdCardViewModel { get; set; }
        public IList<RegistrationStatusViewModel> RegistrationStatusViewModels { get; set; }
        public RegistrationStatusViewModel RegistrationStatusViewModel { get; set; }
        public bool IsAdmitted { get; set; }
    }
}