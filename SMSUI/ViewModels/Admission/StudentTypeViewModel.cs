﻿namespace SMSUI.ViewModels
{
    public class StudentTypeViewModel
    {
        public int StudentTypeId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}