﻿namespace SMSUI.ViewModels
{
    public class StudentContactPersonViewModel
    {
        public int StudentContactPersonId { get; set; }
        public int StudentId { get; set; }
        public int ContactPersonId { get; set; }
        public StudentViewModel StudentViewModel { get; set; }
        public ContactPersonViewModel ContactPersonViewModel { get; set; }
        public bool IsActive { get; set; }
    }
}