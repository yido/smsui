﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
    public class AllStudentsForAdmissionViewModel
    {
        public IList<StudentsForRegistrationViewModel> StudentsForRegistrationViewModels { get; set; }
        public IList<RegistrationStatusViewModel> RegistrationStatusViewModel { get; set; }
        public IList<StudentLevelTypeViewModel> StudentLevelType { get; set; }
        public IList<StudentTypeViewModel> StudentType { get; set; }
    }
}