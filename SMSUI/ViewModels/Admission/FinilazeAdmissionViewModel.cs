﻿using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels
{
    public class FinilazeAdmissionViewModel
    {
        public StudentViewModel StudentViewModel { get; set; }
        public IList<GradeLevelViewModel> PossibleGradeLevelViewModels { get; set; }
        public GradeLevelViewModel SelectedGradeLevelViewModel { get; set; }
        public List<ClassViewModel> RelatedClassViewModels { get; set; }
        public IList<RegistrationStatusViewModel> RegistrationStatusViewModels { get; set; }
    }
}