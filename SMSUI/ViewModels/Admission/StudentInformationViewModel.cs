﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
    public class StudentInformationViewModel
    {
        public StudentInformationViewModel()
        {
            StudentViewModel= new StudentViewModel();
            StudentLevelTypesModels = new List<StudentLevelTypeViewModel>();
            StudentTypeViewModels = new List<StudentTypeViewModel>();
        }
        public StudentViewModel StudentViewModel { get; set; }
        public IList<StudentLevelTypeViewModel> StudentLevelTypesModels { get; set; }
        public IList<StudentTypeViewModel> StudentTypeViewModels { get; set; }
    }
}