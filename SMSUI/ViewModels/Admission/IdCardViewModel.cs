﻿namespace SMSUI.ViewModels
{
    public class IdCardViewModel
    {
        public string PrintedId { get; set; }
        public string SchoolLogoUrl { get; set; }
        public string StudentFirstName { get; set; }
        public string StudentMiddleName { get; set; }
        public string StudentLastName { get; set; }
        public string GradeLevelName { get; set; }
        public string ClassName { get; set; }
        public bool IsAdmitted { get; set; }
        public bool IsActive { get; set; }
    }
}