﻿namespace SMSUI.ViewModels
{
    public class RoleViewModel
    {
        public int RoleID { get; set; }
        public string Name { get; set; }
        public string RoleCode { get; set; }
    }
}