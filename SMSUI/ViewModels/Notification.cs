﻿namespace SMSUI.ViewModels
{
    public class Notification
    {
        public int AnnouncementID { get; set; }
        public string From { get; set; }
        public string Message {get;set;}
        public int Priority { get; set; }
        public string Path { get; set; }
        public string TimeAgo { get; set; }
        public bool seen { get; set; }
    }
}
