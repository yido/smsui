﻿using System;
using System.Collections.Generic;
namespace SMSUI.ViewModels
{
    public class LoggedInUserViewModel
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string PhotoPath { get; set; }
        public int RoleID { get; set; }
        public List<MenuGroupViewModel> MenuGroups { get; set; }
        public AddressViewModel AddressViewModel { get; set; }
        public DemographicInfoViewModel DemographicInfoViewModel { get; set; }
        public List<Notification> Notifications { get; set; }
        public string MiddleName { get; set; }
        public string PassWord { get; set; }
        public DateTime LastLogin { get; set; }

    }
}
