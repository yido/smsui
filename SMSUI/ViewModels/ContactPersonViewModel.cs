﻿namespace SMSUI.ViewModels
{
    public class ContactPersonViewModel
    {
        public int ContactPersonId { get; set; }
        public string Sex { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Relationship { get; set; }
        public bool IsParent { get; set; }
        public bool IsActive { get; set; }
        public int AddressId { get { return (AddressViewModel !=null)? AddressViewModel.AddressId:-1; } }
        public AddressViewModel AddressViewModel { get; set; }
    }
}