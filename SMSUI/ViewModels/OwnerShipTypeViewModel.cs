﻿namespace SMSUI.ViewModels
{
    public class OwnerShipTypeViewModel
    {
        public int OwnerShipTypeId { get; set; }
        public string Name { get; set; }
    }
}
