﻿using System;

namespace SMSUI.ViewModels.Attendance
{
    public class AttendanceViewModel
    {
        public int StudentBatchID { get; set; }
        public int ReasonID { get; set; }
        public DateTime Date { get; set; }
    }
}