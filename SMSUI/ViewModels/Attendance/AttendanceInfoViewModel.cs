﻿using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.ViewModels.Attendance
{
    public class AttendanceInfoViewModel
    {
        public IList<AbsentReason> Reasons { get; set; }
        public IList<GradeLevelViewModel> GradeLevels { get; set; }
        public IList<ClassViewModel> Classes { get; set; }
        public IList<StudentBathViewModel> Student { get; set; }
        public IList<AttendanceViewModel> Attendances { get; set; }
    }
}