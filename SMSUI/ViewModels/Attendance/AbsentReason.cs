﻿namespace SMSUI.ViewModels.Attendance
{
    public class AbsentReason
    {
        public int AbsentReasonID { get; set; }
        public string Name { get; set; }

    }
}