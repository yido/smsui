﻿using System;

namespace SMSUI.ViewModels
{
    public class UserViewModel
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string PhotoPath { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string Department { get; set; }
        public AddressViewModel AddressViewModel { get; set; }
        public DemographicInfoViewModel DemographicInfoViewModel { get; set; }
        public string MiddleName { get; set; }
        public string PassWord { get; set; }
        public DateTime LastLogin { get; set; }
    }
}