﻿using System;

namespace SMSUI.ViewModels
{
    public class AccountsViewModel
    {
        public LoggedInUserViewModel LoggedInUser
        {
            get
            {
                return new LoggedInUserViewModel
                {
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    Lastname = Lastname,
                    RoleID = RoleId,
                    UserName = UserName,
                    LastLogin = DateTime.Now
                };
            }
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Lastname { get; set; }
        public string PhotoPath { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime LastLogin { get; set; }
        public int Activity { get; set; }
        public bool IsActive { get; set; }
    }
}