﻿namespace SMSUI.ViewModels
{
    public class MenuViewModel
    {
        public string Icon { get; set; }
        public int MenuGroupID { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
    }
}
