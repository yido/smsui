﻿namespace SMSUI.ViewModels.Announcement
{
    public class MailDetailViewModel
    {
        public AnnouncementViewModel AnnouncementViewModel { get; set; }
        public int InBoxMails { get; set; }
        public string FakeSetting { get; set; }
    }
}