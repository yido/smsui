﻿using System.Collections.Generic;

namespace SMSUI.ViewModels.Announcement
{
    public class ComposeMailViewModel
    {
        public ComposeMailViewModel()
        {
            RoleViewModels = new List<RoleViewModel>();
            UserViewModels = new List<UserViewModel>();
        }

        public List<RoleViewModel> RoleViewModels { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public List<UserViewModel> UserViewModels { get; set; }
    }
}