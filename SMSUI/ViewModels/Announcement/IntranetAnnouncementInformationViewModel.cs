﻿using System.Collections.Generic;
using System.Linq;

namespace SMSUI.ViewModels.Announcement
{
    public class IntranetAnnouncementInformationViewModel
    {
        public int UserID { get; set; }
        public int InBoxMails { get { return Announcements.Count(m => m.IsSeen == false); }}
        public List<AnnouncementViewModel> Announcements { get; set; }
    }
}