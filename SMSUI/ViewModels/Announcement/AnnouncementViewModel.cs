﻿using System;
using System.Linq;
using System.Web;

namespace SMSUI.ViewModels.Announcement
{
    public class AnnouncementViewModel
    {
        public int AnnouncementID { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public string AttachmentUrl { get; set; }
        public bool IsSeen { get; set; }
    }
}