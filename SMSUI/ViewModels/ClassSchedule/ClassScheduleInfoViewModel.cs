﻿using System.Collections;
using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.ClassSchedule
{
    public class ClassScheduleInfoViewModel
    {
        public IList<GradeLevelViewModel> GradeLevels { get; set; }
    }
}