﻿using System.Collections;
using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.ClassSchedule
{
    public class ClassSchedule
    {
        public IList<SubjectViewModel> Subjects { get; set; }
        public string ValidationSetting { get; set; }
        public string[][] Schedules { get; set; }

    }
}