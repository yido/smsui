﻿namespace SMSUI.ViewModels
{
    public class UserAccountViewModel
    {
        public LoggedInUserViewModel LoggedInUser { get; set; }
        public bool IsActive { get; set; }
    }
}