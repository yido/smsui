﻿using System;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class AcademicYearViewModel
    {
        public int AcademicYearId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
    }
}