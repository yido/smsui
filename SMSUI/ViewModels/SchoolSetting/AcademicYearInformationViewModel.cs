﻿using System.Collections.Generic;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class AcademicYearInformationViewModel
    {
        public List<AcademicYearViewModel> AcademicYearViewModels { get; set; }
        public List<TermViewModel> TermViewModels { get; set; }
        public AcademicYearViewModel CurrentAcademicYearViewModel { get; set; }
    }
}