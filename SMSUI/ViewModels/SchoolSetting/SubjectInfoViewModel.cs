﻿using System.Collections.Generic;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class SubjectInfoViewModel
    {
        public SubjectViewModel SubjectViewModel { get; set; }
        public IList<SubjectTypeViewModel> SubjectTypes { get; set; }
    }
}