﻿using System.Collections.Generic;
using System.Linq;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class GradeLevelViewModel
    {
        public int GradeLevelId { get; set; }
        public string Name { get; set; }
        public int NumberOfClass { get { return ClassViewModels.Count; } }
        public int MaxStudentAllowed
        {
            get
            {
                return ClassViewModels.Sum(Class => Class.MaxNumberOfStudents);
            }
        }

        public StudentLevelTypeViewModel StudentLevelTypeViewModel { get; set; }
        public int CurrentNoOfStudents
        {
            get { return ClassViewModels.Sum(Class => Class.CurrentNoOfStudents); }
        }

        public int RemainingSpace
        {
            get { return (CurrentNoOfStudents*100)/MaxStudentAllowed; }
        }

        public bool IsActive { get; set; }
        public List<ClassViewModel> ClassViewModels { get; set; }

        public string ClassesString
        {
            get
            {
                return ClassViewModels.Aggregate(string.Empty, (index, str) => index + string.Format("{0}{1}", str.Name, ',')).TrimEnd(','); }
        }
    }
}