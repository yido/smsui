﻿namespace SMSUI.ViewModels.SchoolSetting
{
    public class SubjectTypeViewModel
    {
        public int SubjectTypeId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}