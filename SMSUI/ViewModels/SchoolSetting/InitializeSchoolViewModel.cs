﻿namespace SMSUI.ViewModels.SchoolSetting
{
    public class InitializeSchoolViewModel
    {
        public SchoolInformationViewModel SchoolInformationViewModel { get; set; }
        public AcademicYearInformationViewModel AcademicYearInformationViewModel { get; set; }
        public SubjectGradeLevelInformationViewModel SubjectGradeLevelInformationViewModel { get; set; }
        public FinalizeSchoolInitializationViewModel FinalizeSchoolInitializationViewModel { get; set; }
    }
}