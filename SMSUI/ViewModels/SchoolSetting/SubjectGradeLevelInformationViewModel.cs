﻿using System.Collections.Generic;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class SubjectGradeLevelInformationViewModel
    {
        public List<SubjectViewModel> SubjectViewModels { get; set; }
        public List<GradeLevelViewModel> GradeLevelViewModels { get; set; }
        public List<SubjectGradeLevelViewModel> SubjectGradeLevelViewModels { get; set; }
    }
}