﻿namespace SMSUI.ViewModels.SchoolSetting
{
    public class CalenderTypeViewModel
    {
        public int CalenderTypeId { get; set; }
        public string Name { get; set; }
    }
}