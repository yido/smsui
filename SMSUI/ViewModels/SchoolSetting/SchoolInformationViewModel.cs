﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class SchoolInformationViewModel
    {
        [Required]
        public string SchoolName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime EstablishmentDate { get; set; }
        public int SchoolTypeID { get; set; }
        public int OwnerShipTypeID { get; set; }
        public int CalenderTypeID{ get; set; }
        public List<SchoolTypeViewModel> SchoolTypeViewModels { get; set; }
        public List<OwnerShipTypeViewModel> OwnerShipTypeViewModels { get; set; }
        public List<CalenderTypeViewModel>  CalenderTypeViewModels { get; set; }
        public List<AddressViewModel> SchoolAddressViewModels { get; set; }
        public string SchoolLogoPath { get; set; }

    }
}