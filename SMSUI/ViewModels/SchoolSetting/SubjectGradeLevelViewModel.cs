﻿using System.Collections.Generic;

namespace SMSUI.ViewModels.SchoolSetting
{
    public class SubjectGradeLevelViewModel
    {
        public int SubjectGradeLevelId { get; set; }
        public GradeLevelViewModel GradeLevelViewModel { get; set; }
        public List<SubjectViewModel> SubjectViewModels { get; set; }
    }
}