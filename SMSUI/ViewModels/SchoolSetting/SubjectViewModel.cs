﻿namespace SMSUI.ViewModels.SchoolSetting
{
    public class SubjectViewModel
    {
        public int SubjectId { get; set; }
        public string Name { get; set; }
        public SubjectTypeViewModel SubjectTypeViewModel { get; set; }
        public bool IsActive { get; set; }
    }
}