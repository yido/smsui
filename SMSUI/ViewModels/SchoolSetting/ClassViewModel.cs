﻿namespace SMSUI.ViewModels.SchoolSetting
{
    public class ClassViewModel
    {
        public int ClassId { get; set; }
        public string Name { get; set; }
        public int MaxNumberOfStudents { get; set; }
        public int CurrentNoOfStudents { get; set; }
        public bool IsActive { get; set; }
        public GradeLevelViewModel GradeLevelViewModel { get; set; }



        public string StatusWithName 
        {
            get { return Name +" - "+ CurrentNoOfStudents+"/"+ MaxNumberOfStudents; }
        }
    }
}