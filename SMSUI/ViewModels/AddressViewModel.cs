﻿namespace SMSUI.ViewModels
{
    public class  AddressViewModel
    {
        public int AddressId { get; set; }
        public AddressTypeViewModel AddressTypeViewModel { get; set; }
        public string BlockName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string IdentityNumber { get; set; }
        public string Kebele { get; set; }
        public string MobilePhone { get; set; }
        public string OfficePhone { get; set; }
        public string StreetName { get; set; }
        public string SubCity { get; set; }
        public bool IsActive { get; set; }
    }
}
