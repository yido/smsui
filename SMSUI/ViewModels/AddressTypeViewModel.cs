﻿namespace SMSUI.ViewModels
{
    public class AddressTypeViewModel
    {
        public int AddressTypeId { get; set; }
        public string Name { get; set; }
    }
}