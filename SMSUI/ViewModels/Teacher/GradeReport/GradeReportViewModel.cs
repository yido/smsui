﻿namespace SMSUI.ViewModels.Teacher.GradeReport
{
    public class GradeReportViewModel
    {
        public StudentBathViewModel StudentBatchViewModel { get; set; }
        public int Biology { get; set; }
        public int Chemistry { get; set; }
        public int Maths { get; set; }
        public double Total { get; set; }
        public int Rank { get; set; }
        public string Absence { get; set; }

    }
}