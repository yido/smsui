﻿namespace SMSUI.ViewModels.Teacher.GradeReport
{
    public class StudentGradeReportViewModel
    {

        public int StudentID { get; set; }
        public string Student { get; set; }
        public string IDNumber { get; set; }
        public int Biology { get; set; }
        public int Chemistry { get; set; }
        public int Maths { get; set; }
        public double Total { get; set; }
        public int Rank { get; set; }
    }
}