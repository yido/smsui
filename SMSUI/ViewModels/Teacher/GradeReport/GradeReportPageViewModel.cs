﻿using System.Collections.Generic;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Teacher.GradeReport
{
    public class GradeReportPageViewModel
    {
        public ClassViewModel ClassViewModel { get; set; }
        public TermViewModel TermViewModel { get; set; }
        public SchoolInformationViewModel SchoolInformationViewModel { get; set; }
        public IList<GradeReportViewModel> GradeReportViewModels { get; set; }

    }
}