﻿using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Teacher.GradeReport
{
    public class StudentGradeReportPageViewModel
    {
        public StudentGradeReportPageViewModel()
        {
            StudentGradeReportViewModel = new StudentGradeReportViewModel();
        }

        public SchoolInformationViewModel School { get; set; }
        public string Class { get; set; }
        public string Year { get; set; }

        public StudentGradeReportViewModel StudentGradeReportViewModel { get; private set; }
    }
}