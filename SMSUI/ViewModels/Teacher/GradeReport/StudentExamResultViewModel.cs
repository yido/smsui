﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Staff;

namespace SMSUI.ViewModels.Teacher.GradeReport
{
    public class StudentExamResultViewModel
    {
        public StudentBathViewModel StudentBatchViewModel { get; set; }
        public TeacherBatchViewModel TeacherBatchViewModel { get; set; }

        [DisplayName("Student")]
        public string StudentFullName
        {
            get
            {
                return StudentBatchViewModel.StudentViewModel.StudentFullName;
            }
        }

        public string StudentIDNumber
        {
            get
            {
                return StudentBatchViewModel.StudentViewModel.IdNumber;
            }
        }

        public IList<ExamViewModel> ExamsTaken { get; set; }

        public string TeacherFullName
        {
            get
            {
                return TeacherBatchViewModel.TeacherTerm.Teacher.FullName;
            }
        }

        public double Mark { get; set; }
        public List<ExamResultViewModel> ExamResults { get; set; } 

        public SubjectViewModel SubjectViewModel
        {
            get
            {
                return TeacherBatchViewModel.Subject;
            }
        }

        public ClassViewModel ClassViewModel
        {
            get
            {
                return TeacherBatchViewModel.TeacherTerm.Class;
            }
        }
        public double Total { get { return ExamResults.Sum(p => p.Point); } }
    }
}