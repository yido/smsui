﻿using System;
using SMSUI.ViewModels.Staff;

namespace SMSUI.ViewModels.Teacher.GradeReport
{
    public class ClassResultStatusViewModel
    {
        public int ClassResultStatusID { get; set; }
        public TeacherBatchViewModel  TeacherBatchViewModel { get; set; }
        public DateTime SubmittedDate { get; set; }
        public bool IsConfirmed { get; set; }
        public DateTime ConfirmationDate { get; set; }
    }
}