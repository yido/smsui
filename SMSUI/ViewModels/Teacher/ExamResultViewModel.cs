using System;
using SMSUI.ViewModels.Exam;

namespace SMSUI.ViewModels.Teacher
{
    public class ExamResultViewModel
    {
        public int ExamResultID { get; set; }
        public ExamViewModel Exam { get { return TeacherBatchExamViewModel.Exam; } }
        public StudentBathViewModel StudentBathViewModel { get; set; }
        public TeacherBatchExamViewModel   TeacherBatchExamViewModel { get; set; }
        public double Point { get; set; }
        public DateTime DateOfEntry { get; set; }
        public string ScoredOutOf { get { return Point + "/" + Exam.Mark; } }
    }
}