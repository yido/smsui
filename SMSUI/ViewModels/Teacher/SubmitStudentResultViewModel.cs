﻿using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Staff;

namespace SMSUI.ViewModels.Teacher
{
    public class SubmitStudentResultViewModel
    {
        public List<ClassViewModel> ClassViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(c => c.TeacherTerm.Class).Distinct().ToList();
            }
        }
        public List<SubjectViewModel> SubjectViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(c => c.Subject).Distinct().ToList();
            }
        }

        public List<StudentBathViewModel> StudentBathViewModels
        {
            get
            {
                return ExamResultViewModels.Select(s => s.StudentBathViewModel).Distinct().ToList();
            }
        }

        public List<TeacherBatchViewModel> TeacherBatchViewModels { get; set; }


        private List<TeacherBatchExamViewModel> _teacherBatchExamViewModels { get; set; }
        public List<TeacherBatchExamViewModel> TeacherBatchExamViewModels
        {
            get
            {
                if (_teacherBatchExamViewModels.Count > 0) return _teacherBatchExamViewModels;
                foreach (var teacherBatchViewModel in TeacherBatchViewModels)
                {
                    _teacherBatchExamViewModels.AddRange(teacherBatchViewModel.TeacherBatchExamViewModels);
                }
                return _teacherBatchExamViewModels;
            }
        }

        public List<ExamViewModel> ExamViewModels { get; set; } // Load from server side ... a trip to db doesnt need here // : use ClassID and TeacherBatchExamID
       public List<ExamResultViewModel> ExamResultViewModels { get; set; } 
    }
}