﻿using System.ComponentModel;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Staff;

namespace SMSUI.ViewModels.Teacher
{
    public class StudentResultViewModel
    {
        public int StudentResultID { get; set; }
        public TeacherViewModel Teacher
        {
            get; set;
        }
        public ClassViewModel Class
        {
            get; set;

        }
        public SubjectViewModel Subject
        {
            get; set;
        }
        [DisplayName("Confirmed")]
        public bool IsConfirmed { get; set; }

        public string SubmissionDate { get; set; }
        public string ConfirmationDate { get; set; }

       

    }
}