﻿using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Teacher
{
    public class ClassWithSubjectViewModel
    {
        public List<TeacherBatchExamViewModel> TeacherBatchExamViewModels { get; set; }

        public List<SubjectViewModel> SubjectViewModels
        {
            get
            {
                return TeacherBatchExamViewModels.Select(s => s.TeacherBatch.Subject).Distinct().ToList();
            }
        }

        public List<ClassViewModel> ClassViewModels
        {
            get
            {
                return TeacherBatchExamViewModels.Select(t => t.TeacherBatch.TeacherTerm.Class).Distinct().ToList();
            }
        }

        public List<string> ClassWithSubjectsAggrigated { get; set; } //~Fake One There is no such list ~//
    }
}