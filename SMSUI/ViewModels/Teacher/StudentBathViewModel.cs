﻿using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.ViewModels.Teacher
{
    public class StudentBathViewModel
    {
        public StudentBathViewModel()
        {
            StudentViewModel = new StudentViewModel();
            TermViewModel = new TermViewModel();
            ClassViewModel = new ClassViewModel();
        }
        public int StudentBatchID  { get; set; }
        public int StudentID { get { return StudentViewModel.StudentId; } }
        public StudentViewModel StudentViewModel { get; set; }
        public TermViewModel TermViewModel { get; set; }
        public ClassViewModel ClassViewModel { get; set; }

    }
}