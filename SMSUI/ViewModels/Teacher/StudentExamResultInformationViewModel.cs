﻿using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.Teacher.GradeReport;

namespace SMSUI.ViewModels.Teacher
{
    public class StudentExamResultInformationViewModel
    {
        public List<ClassWithSubjectViewModel> ClassWithSubjectAggrigatedViewModel { get; set; }
        public List<ExamViewModel> ExamsTakenWithnAclass { get; set; } //~ Fake One!~//

        public List<StudentExamResultViewModel> StudentExamResults { get; set; }

        //public List<IEnumerable<ExamViewModel>> ExamsTakenWithnAclass
        //{
        //    get
        //    {
        //        return
        //            ClassWithSubjectAggrigatedViewModel.Select(
        //                t => t.TeacherBatchExamViewModels.ToList().Select(e => e.Exam))
        //                .Distinct()
        //                .ToList();
        //    }
        //}


    }
}