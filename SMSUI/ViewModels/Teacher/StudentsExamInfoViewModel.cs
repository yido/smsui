﻿using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Staff;

namespace SMSUI.ViewModels.Teacher
{
    public class StudentsExamInfoViewModel
    {
        public StudentsExamInfoViewModel()
        {
            _teacherBatchExamViewModels = new List<TeacherBatchExamViewModel>();
            TeacherBatchViewModels = new List<TeacherBatchViewModel>();
        }

        public List<ClassViewModel> ClassViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(c => c.TeacherTerm.Class).Distinct().ToList();
            }
        }
        public List<SubjectViewModel> SubjectViewModels
        {
            get
            {
                return TeacherBatchViewModels.Select(c => c.Subject).Distinct().ToList();
            }
        }

        public List<StudentBathViewModel> StudentBathViewModels { get; set; }
        public List<TeacherBatchViewModel> TeacherBatchViewModels { get; set; }


        private List<TeacherBatchExamViewModel> _teacherBatchExamViewModels { get; set; }
        public List<TeacherBatchExamViewModel> TeacherBatchExamViewModels
        {
            get
            {
                if (_teacherBatchExamViewModels.Count > 0) return _teacherBatchExamViewModels;
                foreach (var teacherBatchViewModel in TeacherBatchViewModels)
                {
                    _teacherBatchExamViewModels.AddRange(teacherBatchViewModel.TeacherBatchExamViewModels);
                }
                return _teacherBatchExamViewModels;
            }
        }



        public double Point { get; set; }
        public ClassViewModel ClassViewModel { get; set; }
        public StudentBathViewModel StudentBathViewModel { get; set; }
        public TeacherBatchViewModel TeacherBatchViewModel { get; set; }
        public TeacherBatchExamViewModel TeacherBatchExamViewModel { get; set; }
    }
}