﻿using System.Collections.Generic;

namespace SMSUI.ViewModels.Teacher
{
    public class MyStudentsViewModel
    {
        public StudentsExamInfoViewModel StudentsExamInfoViewModel { get; set; }
        public List<ExamResultViewModel> ExamResultViewModels { get; set; }
        public StudentExamResultInformationViewModel StudentExamResultInformationViewModel { get; set; }
    }
}