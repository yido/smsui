﻿using System.Collections.Generic;
using System.Linq;

namespace SMSUI.ViewModels
{
    public class AccountEditViewModel
    {
        public AccountsViewModel Account { get; set; }
        public List<RoleViewModel> Roles { get; set; }

        public RoleViewModel CurrentAccountRole
        {
            get { return Roles.FirstOrDefault(roleId => roleId.RoleID == Account.RoleId); }
        }
    }
}