﻿using System.Collections.Generic;

namespace SMSUI.ViewModels
{
     public class MenuGroupViewModel
    {
         public string Icon { get; set; }
         public List<MenuViewModel> Menus { get; set; }
         public string Name { get; set; }
         public string URL { get; set; }
    }
}
