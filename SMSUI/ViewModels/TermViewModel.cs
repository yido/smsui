﻿using System;

namespace SMSUI.ViewModels
{
    public class TermViewModel
    {
        public int TermId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }

        public int Consumed
        {
            get
            {
                if (DateTime.Now <= StartDate) return 0;
                if (DateTime.Now >= EndDate) return 100;
                return (DateTime.Now.Subtract(StartDate).Days * 100) / EndDate.Subtract(StartDate).Days;
            }
        }
    }
}