﻿using System.Linq;
using System.Web.Mvc;
using SMSUI.ViewModels;
using System;
using System.Collections.Generic;
using SMSUI.ViewModels.Setting;

namespace SMSUI.Repository
{
    public class FakeService
    {
        public  List<ContactPersonViewModel> ContactPersonViewModels = new List<ContactPersonViewModel>();
        private readonly FakeAnnouncementService _announcementService = new FakeAnnouncementService();

        public PeriodSettingViewModel PeriodSettingViewModel { get; set; }

        public FakeService()
        {
            AddFakeContactPersons();
            GetPeriodSetting();
        }
        public LoggedInUserViewModel GetFakeAdminUser()
        {
            var adminUser = new LoggedInUserViewModel
            {
                UserID= 1,
                UserName ="yidogeb",
                FirstName="Yididiya",
                MiddleName ="Gebredingel",
                Lastname="Berhe",
                PassWord = "123456",
                PhotoPath = "../Content/img/Jared.jpg",
                RoleID = 1,
                AddressViewModel = GetAddress(),
                DemographicInfoViewModel = GetDemographicInformationUser(),
                MenuGroups = GetMenuGroupsForAdmin(),
                Notifications = GetNotifications(),
                LastLogin = DateTime.Parse("2014-10-04 20:39:15.523")
            };
            return adminUser;
        }

        private List<Notification> GetNotifications()
        {
            var announcements = _announcementService.GetIntranetAnnoucementInfo().Announcements.Where(s=>s.IsSeen == false);

            return announcements.Select(announcement => new Notification
            {
                AnnouncementID =announcement.AnnouncementID,
                From = announcement.From,
                Priority = 0,
                Message = announcement.Subject,
                TimeAgo = (announcement.AnnouncementID + 1) + " sec",
                seen = announcement.IsSeen,
                Path = GetRandomPhoto(announcement.AnnouncementID)
            }).ToList();

            //return new List<Notification>
            //{
            //    {new Notification{ From="Director",Priority=0,Message="Please summit your report ASAP!",TimeAgo="3 sec",seen = false,Path="../../Content/img/Best.jpg"}},
            //    {new Notification{ From="Admin",Priority=0,Message="Please change your password!",TimeAgo="17 mins",seen = true, Path="../../Content/img/Jared.jpg"}}
            //};
        }

        private string GetRandomPhoto(int announcementID)
        {
            if (announcementID%2 == 0)
            {
                return "../../Content/img/Jared.jpg";
            }

            return "../../Content/img/default-avatar.png";
        }

        private List<MenuGroupViewModel> GetMenuGroupsForAdmin()
        {
            var adminMenuGroups = new  List<MenuGroupViewModel>()
          {         { new MenuGroupViewModel
                                {
                                    Name = "School Settings",
                                    Icon = "li_settings", 
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="School Information",Icon="li_note",URL=""}},{new MenuViewModel{Name="School Initialization",Icon="li_fire",URL="/Views/School/Index"}},{new MenuViewModel{Name="User Account",Icon="fa fa-group",URL="/Views/Account/Index"}}}
                                }
                    },
                    { new MenuGroupViewModel
                                {
                                    Name = "Admission",
                                    Icon = "li_key",
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="Registration",Icon="li_pen",URL="/Views/Admission/Index"}},{new MenuViewModel{Name="Admission",Icon="li_like",URL="/Views/Admission/AllStudentsForAdmissioin"}}}
                                }
                    }, 
                    { new MenuGroupViewModel
                                {
                                    Name = "Staff",
                                    Icon = "li_eye",
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="Teachers",Icon="li_paperplane",URL="/Views/Teacher/Index"}},{new MenuViewModel{Name="Teacher Batch",Icon="li_paperplane",URL="/Views/Teacher/_TeacherBatch"}}}
                                }
                    }, 
                    { new MenuGroupViewModel
                                {
                                    Name = "Teacher",
                                    Icon = "li_user",
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="My students",Icon="li_heart",URL="/Views/MyStudents/Index"}},{new MenuViewModel{Name="Grade Report",Icon="li_data",URL="/Views/GradeReport/Index"}},new MenuViewModel{Name="Attendance",Icon="li_clock",URL="/Views/Attendance/Index"},new MenuViewModel{Name="Exam",Icon="li_paperplane",URL = "/Views/Exam/Index"}}
                                    
                                }
                    },
                    { new MenuGroupViewModel
                                {
                                    Name = "Students",
                                    Icon = "fa fa-group" ,
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="ID-Reprint",Icon="li_vallet",URL=""}}}
                                }
                    }, 
                    { new MenuGroupViewModel
                                {
                                    Name = "Schedule",
                                    Icon = "li_calendar",
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="Class Schedule",Icon="li_calendar",URL="/Views/ClassSchedule/Index"}},{new MenuViewModel{Name="Exam Schedule",Icon="li_vallet",URL="/Views/ExamSchedule/Index"}},{new MenuViewModel{Name="Create Event",Icon="li_pen",URL=""}},{new MenuViewModel{Name="Tasks",Icon="li_tag",URL=""}}}
                                }
                    }, 
                    { new MenuGroupViewModel
                                {
                                    Name = "Announcement",
                                    Icon = "li_bulb",
                                    URL ="",
                                    Menus = new List<MenuViewModel>{{new MenuViewModel{Name="Mobile SMS",Icon="li_phone",URL=""}},{ new MenuViewModel{Name="Intranet",Icon="li_mail",URL="/Views/Announcement/Index"}}}
                                }
                    }, 
                    { new MenuGroupViewModel
                                {
                                    Name = "DashBoard",
                                    Icon = "fa fa-dashboard",
                                    URL ="",
                                    Menus = new List<MenuViewModel>{}
                                }
                    }
            }; 
            return adminMenuGroups;
        }

        public static AddressViewModel GetAddress()
        {
            return new AddressViewModel
            {  AddressId = 1,
                IdentityNumber = "Address1",
                Country = "Ethiopia",
                City = "Addis Ababa",
                SubCity = "Gulele",
                Kebele = "07/17",
                Email = "yidogeb@gmail.com",
                MobilePhone = "+251913585396",
                AddressTypeViewModel = new AddressTypeViewModel { AddressTypeId = 1,Name = "Office"}
            };
        }
        public static AddressViewModel GetContactPersonAddress()
        {
            return new AddressViewModel
            {   AddressId = 1,
                IdentityNumber = "00871",
                Country = "Ethiopia",
                City = "Addis Ababa",
                SubCity = "Bole",
                Kebele = "02/51",
                Email = "baki@gmail.com",
                MobilePhone = "+251913827302",
                AddressTypeViewModel = new AddressTypeViewModel { AddressTypeId = 1, Name = "Home" }
            };
        }
        public static DemographicInfoViewModel GetDemographicInformationUser()
        {
            return new DemographicInfoViewModel
            {
                DemographicInfoId = 1,
                Sex = "Male",
                DateOfBirth = new DateTime(1988, 3, 14),
                Nationality = "Ethiopian",
                PlaceOfBirth = "Ethiopia",
                Age = (int) (DateTime.Now.Subtract(new DateTime(1988, 3, 14)).TotalDays/365),
                Height = (decimal) 1.69,
                Length = 179,
                Weight = 48,
                BloodGroup = "AB",
                Religion = "Muslim"
                

            };
        }

        public static DemographicInfoViewModel GetDemographicInformation() 
        {
            return new DemographicInfoViewModel
            {   DemographicInfoId = 1,
                Sex = "Female",
                DateOfBirth = new DateTime(1978, 1, 6),
                Nationality = "Ethiopian",
                Age = (int)(DateTime.Now.Subtract(new DateTime(1978, 1, 6)).TotalDays / 365),
            };
        }

        public void AddFakeContactPersons()
        {
            ContactPersonViewModels.Add(new ContactPersonViewModel
            {
                ContactPersonId = 1,
                Relationship = "Father",
                Sex = "Male",
                FirstName = "Mentesnot",
                MiddleName = "Tadesse",
                LastName = "Solomon",
                JobTitle = "Driver",
                IsParent = true,
                IsActive = true,
            });
            //ContactPersonViewModels.Add(new ContactPersonViewModel
            //{
            //    ContactPersonId = 1,
            //    Relationship = "Mother",
            //    Sex = "Female",
            //    FirstName = "Selamawit",
            //    MiddleName = "Reta",
            //    LastName = "",
            //    JobTitle = "",
            //    IsParent = true,
            //    IsActive = true,
            //});
        }

        public IList<PeriodViewModel> GetPeriods()
        { 
            const int numberOfPeriods = 8;
            var periods = new List<PeriodViewModel>();
            for (int i = 1; i <= numberOfPeriods; i++)
            {
                var startTime = PeriodSettingViewModel.ClassStartTime;
                periods.Add( new PeriodViewModel
                {
                    PeriodID = i,
                    Name = "Period "+ i,
                    StartTime = i==1? startTime:(startTime + new TimeSpan(0,45,0)),
                    EndTime = new TimeSpan(3,10,0),

                });
            }
            periods.Add(new PeriodViewModel
            {
                Name = "Morning Break",
                PeriodID = 9,
                StartTime = new TimeSpan(6, 20, 0),
                EndTime = new TimeSpan(7, 20, 0),
            });
            periods.Add(new PeriodViewModel
            {
                Name = "Lunch Break",
                PeriodID = 10,
                StartTime = new TimeSpan(6, 20, 0),
                EndTime = new TimeSpan(7, 20, 0),
            });
            
            return periods.ToList();
        }


        public PeriodSettingViewModel GetPeriodSetting()
        {
            return PeriodSettingViewModel = new PeriodSettingViewModel
            {
                WorkingHour = 480, //8*60 minutes 
                PeriodDuration = 45,
                ClassStartTime = new TimeSpan(2, 30, 0),
                MorningBreakDuration = 15,
                MorningBreakStartTime = new TimeSpan(4, 30, 0),
                LunchBreakDuration = 60,
                LunchBreakStartTime = new TimeSpan(6, 30, 0),
                ClassEndTime = new TimeSpan(9, 45, 0)

            };
        }
    }
}