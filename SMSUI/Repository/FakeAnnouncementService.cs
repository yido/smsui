﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMSUI.ViewModels;
using SMSUI.ViewModels.Announcement;

namespace SMSUI.Repository
{
    public class FakeAnnouncementService
    {
        public List<UserViewModel> UserViewModels = new List<UserViewModel>();
        public IntranetAnnouncementInformationViewModel GetIntranetAnnoucementInfo()
        {
            return new IntranetAnnouncementInformationViewModel
            {
                UserID = 1,
                Announcements = GetAnnouncements()
            };
        }
        public List<AnnouncementViewModel> GetIntranetSentAnnoucementInfo()
        {
           return new List<AnnouncementViewModel>
           {
                 new AnnouncementViewModel
                {
                    AnnouncementID = 1,
                    From = "Yididiya G.",
                    To = "Yared D., Yonas T. ,Solomon A. ,Selam G. ,King Y. ,Zerihun K.",
                    Date = DateTime.Now.Date,
                    Subject = "Change Passsword!",
                    Message =@"Hi All, Please make sure that you all have changed your former passwords! Thanks.",
                    IsSeen = false
                },
                new AnnouncementViewModel
                {
                    AnnouncementID = 2,
                    From = "Yididiya G.",
                    To = "Chala G. ,Gebeyehu T. ,Simon B. ,Hailemariam H. ,Adane B. ,Fasil D. , Aschalewu k. ,Serke K.",
                    Date = DateTime.Now.Date,
                    Subject = "Please Pay your monthly tution fee!",
                    Message =@"Dear parents, we are kindly informing you to pay monthly tution fee as soon as possible. Thanks.",
                    IsSeen = true
                },
                new AnnouncementViewModel
                {
                    AnnouncementID = 3,
                    From = "Yididiya G.",
                    To = "Chala G. ,Gebeyehu T. ,Simon B. ,Hailemariam H. ,Adane B. ,Fasil D. , Aschalewu k. ,Serke K.",
                    Date = DateTime.Now.Date,
                    Subject = "Please Pay your monthly tution fee!",
                    Message =@"Dear parents, we are kindly informing you to pay monthly tution fee as soon as possible. Thanks.",
                    IsSeen = false
                }
           };
        }
        private List<AnnouncementViewModel> GetAnnouncements()
        {
            return new List<AnnouncementViewModel>
            {
                new AnnouncementViewModel
                {
                    AnnouncementID = 1,
                    From = "Yared D.",
                    Date = DateTime.Now.Date,
                    Subject = "Payment Report!",
                    Message =@"Hi Yididya we need to get all the payment reports by next week; as we will need to summit to the goverment officials by Jan 1st! Thanks.",
                    IsSeen = false
                },
                new AnnouncementViewModel
                {
                    AnnouncementID = 2,
                    From = "Jonas T.",
                    Date = DateTime.Now.Date,
                    Subject = "May you call me Asap!",
                    Message =@"Hi Yididya please find my attachments on the link below and make sure you have used all the technologies I have listed below. Thanks.",
                    IsSeen = true
                },
                new AnnouncementViewModel
                {
                    AnnouncementID = 3,
                    From = "Selam G.",
                    Date = DateTime.Now.Date,
                    Subject = "Check Notice Board!",
                    Message =@"Hi Just to inform you if you didnt got a chance to pass by Notice Board. Thanks.",
                    IsSeen = true
                },
                new AnnouncementViewModel
                {
                    AnnouncementID = 4,
                    From = "Admin",
                    Date = DateTime.Now.Subtract(new TimeSpan(4)),
                    Subject = "Salary Issues!",
                    Message =@"Hi All Teachers, Please be informed that we will no longer use a manual cash payment system from today Feb 10! Thanks.",
                    IsSeen = false
                }


            };

        }

        public ComposeMailViewModel GetComposeMailInfo()
        {
            return new ComposeMailViewModel
            {
                Message = "",
                Subject = "",
                RoleViewModels = GetAllRoles(),
                UserViewModels = GetSampleUsers()
            };
        }

        public List<UserViewModel> GetSampleUsers()
        {
            UserViewModels = new List<UserViewModel>
            {
                new UserViewModel
            {
                UserID = 1,
                UserName = "yidogeb",
                FirstName = "Yididiya",
                MiddleName = "Gebredingel",
                Lastname = "Berhe",
                PassWord = "123456",
                RoleName ="Admin",
                Department ="Math",
                AddressViewModel = GetAddress(),
                DemographicInfoViewModel = GetDemographicInformationUser(),
                LastLogin = DateTime.Parse("2014-10-04 20:39:15.523"),
                RoleID = 1
            }, new UserViewModel
            {
                UserID = 2,
                UserName = "yareddej",
                FirstName = "Yared",
                MiddleName = "Dejene",
                Lastname = "Dessaleh",
                RoleName ="Admin",
                Department ="Math",
                RoleID = 1,
            }, new UserViewModel
            {
                UserID = 3,
                UserName = "shalomG",
                FirstName = "Shalom",
                MiddleName = "Gebredingel",
                Lastname = "Berhe",
                RoleName ="Director",
                Department ="Managment",
                RoleID = 2
            }, new UserViewModel
            {
                UserID = 4,
                UserName = "vuvas57",
                FirstName = "Yonas",
                MiddleName = "Tesfaye",
                Lastname = "Worku",
                RoleName ="Director",
                Department ="Managment",
                RoleID = 2
            },
            };
            return UserViewModels;
        }
        public static AddressViewModel GetAddress()
        {
            return new AddressViewModel
            {
                AddressId = 1,
                IdentityNumber = "Address1",
                Country = "Ethiopia",
                City = "Addis Ababa",
                SubCity = "Gulele",
                Kebele = "07/17",
                Email = "yidogeb@gmail.com",
                MobilePhone = "+251913585396",
                AddressTypeViewModel = new AddressTypeViewModel { AddressTypeId = 1, Name = "Office" }
            };
        }
        public static DemographicInfoViewModel GetDemographicInformationUser()
        {
            return new DemographicInfoViewModel
            {
                DemographicInfoId = 1,
                Sex = "Male",
                DateOfBirth = new DateTime(1988, 3, 14),
                Nationality = "Ethiopian",
                PlaceOfBirth = "Ethiopia",
                Age = (int)(DateTime.Now.Subtract(new DateTime(1988, 3, 14)).TotalDays / 365),
                Height = (decimal)1.69,
                Length = 179,
                Weight = 48,
                BloodGroup = "AB",
                Religion = "Muslim"


            };
        }

        public List<RoleViewModel> GetAllRoles()
        {
            return new List<RoleViewModel>
            {
                {new RoleViewModel {RoleID = 1, Name = "System Administrator"}},
                {new RoleViewModel {RoleID = 2, Name = "Director"}},
                {new RoleViewModel {RoleID = 3, Name = "Department Head"}},
                {new RoleViewModel {RoleID = 4, Name = "Program Office"}},
                {new RoleViewModel {RoleID = 5, Name = "Teacher"}}
            };


        }
    }
}