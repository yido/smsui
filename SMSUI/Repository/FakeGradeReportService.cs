﻿using System;
using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Staff;
using SMSUI.ViewModels.Teacher;
using SMSUI.ViewModels.Teacher.GradeReport;

namespace SMSUI.Repository
{
    public class FakeGradeReportService
    {
        #region CLASS RESULT STATUS
        public List<ClassResultStatusViewModel> GetClassResultStatus()
        {
            var teacherService = new FakeTeacherService();
            var classResults = new List<ClassResultStatusViewModel>
            {
                new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 1,
                    TeacherBatchViewModel = teacherService.GeTeacherBatchByID(1),
                    SubmittedDate = DateTime.Now.AddDays(-1),
                    IsConfirmed = true,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                },new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 1,
                    TeacherBatchViewModel = teacherService.GeTeacherBatchByID(1),
                    SubmittedDate = DateTime.Now.AddDays(-1),
                    IsConfirmed = true,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                },new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 1,
                    TeacherBatchViewModel = teacherService.GeTeacherBatchByID(1),
                    SubmittedDate = DateTime.Now.AddDays(-1),
                    IsConfirmed = true,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                },
                new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 4,
                    TeacherBatchViewModel = new TeacherBatchViewModel {TeacherBatchID = 33,
                    TeacherTerm = new TeacherTermViewModel
                    {
                        Class = new FakeSchoolService().GetClassViewModel(91),
                        TeacherTermID = 5,
                    IsSupervisor = false,
                    Teacher = new FakeTeacherService().GetTeacherByID(4),
                    Term = new FakeSchoolService().GetTermViewModel(1)
                    },
                    Subject = new FakeSchoolService().GetSubjectByID(2)
                
                },
                    SubmittedDate = DateTime.Now,
                    IsConfirmed = true,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                },new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 4,
                    TeacherBatchViewModel = new TeacherBatchViewModel {TeacherBatchID = 33,
                    TeacherTerm = new TeacherTermViewModel
                    {
                        Class = new FakeSchoolService().GetClassViewModel(91),
                        TeacherTermID = 5,
                    IsSupervisor = false,
                    Teacher = new FakeTeacherService().GetTeacherByID(4),
                    Term = new FakeSchoolService().GetTermViewModel(1)
                    },
                    Subject = new FakeSchoolService().GetSubjectByID(2)
                
                },
                    SubmittedDate = DateTime.Now,
                    IsConfirmed = true,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                },new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 4,
                    TeacherBatchViewModel = new TeacherBatchViewModel {TeacherBatchID = 33,
                    TeacherTerm = new TeacherTermViewModel
                    {
                        Class = new FakeSchoolService().GetClassViewModel(91),
                        TeacherTermID = 5,
                    IsSupervisor = false,
                    Teacher = new FakeTeacherService().GetTeacherByID(4),
                    Term = new FakeSchoolService().GetTermViewModel(1)
                    },
                    Subject = new FakeSchoolService().GetSubjectByID(2)
                
                },
                    SubmittedDate = DateTime.Now,
                    IsConfirmed = true,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                },
                new ClassResultStatusViewModel
                {
                    ClassResultStatusID = 3,
                    TeacherBatchViewModel = teacherService.GeTeacherBatchByID(3),
                    SubmittedDate = DateTime.Now.AddDays(-2),
                    IsConfirmed = false,
                    ConfirmationDate = DateTime.Now.AddDays(1)
                }
            };
            return classResults;
        }
        public IEnumerable<StudentResultViewModel> GetClassResultStatusByTeacherTerm(TeacherTermViewModel teacherTerm)
        {
            var results =
                GetClassResultStatus()
                    .Where(a => a.TeacherBatchViewModel.TeacherTerm.Class.ClassId == teacherTerm.Class.ClassId).Select(a => new StudentResultViewModel
                    {
                        StudentResultID = a.ClassResultStatusID,
                        Teacher = a.TeacherBatchViewModel.TeacherTerm.Teacher,
                        Subject = a.TeacherBatchViewModel.Subject,
                        Class = a.TeacherBatchViewModel.TeacherTerm.Class,
                        IsConfirmed = a.IsConfirmed,
                        SubmissionDate = a.SubmittedDate.ToShortDateString(),
                        ConfirmationDate = a.ConfirmationDate.ToShortDateString()
                    }).ToList();
            return results;
        }
        public IList<StudentExamResultViewModel> GetExamResultforClass(int classID)
        {
            var studentResult = new List<StudentExamResultViewModel>
            {
                new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(1),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 1)
                        
                    },
                    Mark = 87

                },
                new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(1),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 2)
                    }
                    ,Mark = 90

                },
                new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(1),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 3)
                    }
                    ,Mark = 97

                },new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(1),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 4)
                    }
                    ,Mark = 67
                },
                new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(1),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 5)
                    }
                    ,Mark = 67
                },
                new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(3),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 6)
                    }
                    ,Mark = 67
                },
                new StudentExamResultViewModel
                {
                    TeacherBatchViewModel = new FakeTeacherService().GeTeacherBatchByID(2),
                    StudentBatchViewModel = new StudentBathViewModel
                    {
                        ClassViewModel = new FakeSchoolService().GetClassViewModel(classID),
                        StudentBatchID = 1,
                        StudentViewModel = new FakeAdmissioinService().StudentViewModels.SingleOrDefault(a=>a.StudentId == 7)
                    }
                    ,Mark = 67
                }

            }.Where(a => a.ClassViewModel.ClassId == classID);

            return studentResult.ToList();
        }

        #endregion


        #region GRADE REPORT

        public GradeReportPageViewModel GradeReport()
        {
            var schoolservice = new FakeSchoolService();

            var gradereport = new List<GradeReportViewModel>
            {
                new GradeReportViewModel
                {
                    StudentBatchViewModel =
                        new FakeAdmissioinService().StudentBathViewModels.FirstOrDefault(st => st.StudentID == 1),
                    Biology = 93,
                    Chemistry = 99,
                    Maths = 87,
                    Total = 900,
                    Absence = "",
                    Rank = 1
                },
                new GradeReportViewModel
                {
                    StudentBatchViewModel =
                        new FakeAdmissioinService().StudentBathViewModels.FirstOrDefault(st => st.StudentID == 2),
                    Biology = 93,
                    Chemistry = 99,
                    Maths = 87,
                    Total = 800,
                    Rank = 2
                },
                new GradeReportViewModel
                {
                    StudentBatchViewModel =
                        new FakeAdmissioinService().StudentBathViewModels.FirstOrDefault(st => st.StudentID == 3),
                    Biology = 93,
                    Chemistry = 99,
                    Maths = 87,
                    Total = 780,
                    Rank = 3
                },
                new GradeReportViewModel
                {
                    StudentBatchViewModel =
                        new FakeAdmissioinService().StudentBathViewModels.FirstOrDefault(st => st.StudentID == 4),
                    Biology = 93,
                    Chemistry = 99,
                    Maths = 87,
                    Total = 700,
                    Rank = 5
                },
                new GradeReportViewModel
                {
                    StudentBatchViewModel =
                        new FakeAdmissioinService().StudentBathViewModels.FirstOrDefault(st => st.StudentID == 5),
                    Biology = 93,
                    Chemistry = 99,
                    Maths = 87,
                    Total = 800,
                    Rank = 4
                },
                new GradeReportViewModel
                {
                    StudentBatchViewModel =
                        new FakeAdmissioinService().StudentBathViewModels.FirstOrDefault(st => st.StudentID == 6),
                    Biology = 93,
                    Chemistry = 99,
                    Maths = 87,
                    Total = 800,
                    Rank = 5
                }
            };
            var gradereportPageView = new GradeReportPageViewModel
            {
                ClassViewModel = schoolservice.GetClassViewModel(91),
                SchoolInformationViewModel = schoolservice.GetSchoolInformationViewModel(),
                TermViewModel = schoolservice.GetTermViewModel(1),
                GradeReportViewModels = gradereport
            };
            return gradereportPageView;
        }
        public StudentGradeReportPageViewModel GetStudentGradeReport(int studentId)
        {
            var grade = GradeReport().GradeReportViewModels.SingleOrDefault(a => a.StudentBatchViewModel.StudentID == studentId);


            if (grade != null)
                return new StudentGradeReportPageViewModel
                {
                    School = new FakeSchoolService().GetSchoolInformationViewModel(),
                    Class = "9-A",
                    Year = "First Term/2014",
                    StudentGradeReportViewModel ={
                    StudentID = studentId,
                    Student = grade.StudentBatchViewModel.StudentViewModel.StudentFullName,
                    IDNumber = grade.StudentBatchViewModel.StudentViewModel.IdNumber,
                    Biology = grade.Biology,
                    Chemistry = grade.Chemistry,
                    Maths = grade.Maths,
                    Total = grade.Total,
                    Rank = grade.Rank}
                };
            return new StudentGradeReportPageViewModel();
        }

        #endregion

    }
}