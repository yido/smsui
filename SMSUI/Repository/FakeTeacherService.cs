﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SMSUI.ViewModels;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.Staff;

namespace SMSUI.Repository
{
    public class FakeTeacherService
    {
        #region Teacher

        public IEnumerable<TeacherViewModel> GetAllTeachers()
        {
            var teachers = new List<TeacherViewModel>
            {
                new TeacherViewModel
                {
                    TeacherID = 1,
                    IdNumber = "001",
                    FirstName = "Blen",
                    MiddleName = "Gebru",
                    LastName = "Asmelash",
                    IsActive = true,
                    Address = new AddressViewModel {AddressId = 20, MobilePhone = "+251912127324"},
                    DemographicInformation = FakeService.GetDemographicInformationUser()
                },
                new TeacherViewModel
                {
                    TeacherID = 2,
                    IdNumber = "002",
                    FirstName = "Bereket",
                    MiddleName = "Dagn",
                    LastName = "Asmelash",
                    IsActive = true,
                    Address =
                        new AddressViewModel
                        {
                            AddressId = 20,
                            MobilePhone = "+251912127890",
                            Kebele = "03/04",
                            City = "AA",
                            SubCity = "Kolfe",
                            Email = "somebody@email.com",
                        },
                    DemographicInformation =
                        new DemographicInfoViewModel
                        {
                            DateOfBirth = DateTime.Now,
                            Nationality = "Ethiopian",
                            BloodGroup = "O",
                            Height = 129,
                            Sex = "Male"
                        }
                },
                new TeacherViewModel
                {
                    TeacherID = 3,
                    IdNumber = "003",
                    FirstName = "Abriham",
                    MiddleName = "Wolde",
                    LastName = "Dejene",
                    IsActive = false,
                    Address =
                        new AddressViewModel
                        {
                            AddressId = 20,
                            MobilePhone = "+251911123456",
                            Kebele = "03/04",
                            City = "AA",
                            SubCity = "Yeka",
                            Email = "someone@email.com",
                        },
                    DemographicInformation =
                        new DemographicInfoViewModel
                        {
                            DateOfBirth = DateTime.Now,
                            Nationality = "Ethiopian",
                            BloodGroup = "O",
                            Height = 129,
                            Sex = "Male"
                        }
                },
                new TeacherViewModel
                {
                    TeacherID = 4,
                    IdNumber = "004",
                    FirstName = "Alemu",
                    MiddleName = "Gebru",
                    LastName = "Asmelash",
                    IsActive = true,
                    Address =
                        new AddressViewModel
                        {
                            AddressId = 20,
                            MobilePhone = "+251910102030",
                            Kebele = "03/04",
                            City = "AA",
                            SubCity = "Arada",
                            Email = "someone@email.com",
                        },
                    DemographicInformation =
                        new DemographicInfoViewModel
                        {
                            DateOfBirth = DateTime.Now,
                            Nationality = "Ethiopian",
                            BloodGroup = "O",
                            Height = 129,
                            Sex = "Male"
                        }
                },
                new TeacherViewModel
                {
                    TeacherID = 5,
                    IdNumber = "004",
                    FirstName = "Alex",
                    MiddleName = "Ajaz",
                    LastName = "Gorfu",
                    IsActive = true,
                    Address = FakeService.GetAddress(),
                    DemographicInformation = FakeService.GetDemographicInformation()
                }
            };

            var service = new FakeAccountService();
            var students = service.GetAllAcccounts().Select(a => new TeacherViewModel
            {
                FirstName = a.FirstName,
                MiddleName = a.MiddleName,
                LastName = a.Lastname,
                IsActive = a.IsActive,
                Address = FakeService.GetAddress(),
                DemographicInformation = FakeService.GetDemographicInformationUser(),
                IdNumber = "TCR-" + a.UserId.ToString(CultureInfo.InvariantCulture),
                TeacherID = 500 + a.UserId
            }).ToList();


            teachers.AddRange(students);

            return teachers.ToList();
        }

        public TeacherViewModel GetTeacherByID(int? teacherID)
        {
            if (teacherID == null)
            {
                return new TeacherViewModel
                {
                    Address = new AddressViewModel(),
                    DemographicInformation = new DemographicInfoViewModel()
                };
            }
            return GetAllTeachers().SingleOrDefault(a => a.TeacherID == teacherID);
        }

        #endregion

        #region TeacherTerm

        public IList<TeacherTermViewModel> GetTeacherTermViewModels()
        {
            var schoolservice = new FakeSchoolService();
            return new List<TeacherTermViewModel>
            {
                new TeacherTermViewModel
                {
                    TeacherTermID = 1,
                    Class = schoolservice.GetClassViewModel(91),
                    IsSupervisor = true,
                    Teacher = new FakeTeacherService().GetTeacherByID(1),
                    Term = new FakeSchoolService().GetTermViewModel(1)
                },
                new TeacherTermViewModel
                {
                    TeacherTermID = 2,
                    Class = schoolservice.GetClassViewModel(92),
                    IsSupervisor = true,
                    Teacher = new FakeTeacherService().GetTeacherByID(2),
                    Term = new FakeSchoolService().GetTermViewModel(2)
                },
                new TeacherTermViewModel
                {
                    TeacherTermID = 3,
                    Class = schoolservice.GetClassViewModel(93),
                    IsSupervisor = true,
                    Teacher = new FakeTeacherService().GetTeacherByID(3),
                    Term = new FakeSchoolService().GetTermViewModel(3)
                }
            };
        }

        public TeacherTermViewModel GetTeacherTermViewModel(int teacherTermID)
        {
            return GetTeacherTermViewModels().SingleOrDefault(a => a.TeacherTermID == teacherTermID);
        }

        #endregion

        #region TeacherBatch

        public List<TeacherBatchViewModel> GeAllTeacherBatch()
        {
            var schoolservice = new FakeSchoolService();
            var teacherBatch = new List<TeacherBatchViewModel>
            { new TeacherBatchViewModel
                {
                    TeacherBatchID = 1,
                    TeacherTerm = GetTeacherTermViewModel(1),
                    Subject = schoolservice.GetSubjectByID(1)},
                new TeacherBatchViewModel
                {//~ Phy Teacher on First Term for 9-B Not supervisour ~//
                    TeacherBatchID = 2,
                    Subject = schoolservice.GetSubjectByID(2),
                    TeacherTerm = GetTeacherTermSingleSecond(),
                    TeacherBatchExamViewModels = GetTeacherBatchExamByTeacherBatchID(2)
                
                },new TeacherBatchViewModel
                {//~ Math Teacher on First Term for 11-A Not supervisour ~//
                    TeacherBatchID = 3,
                    TeacherTerm =GetTeacherTermViewModel(3) ,
                    Subject = schoolservice.GetSubjectByID(3),
                    TeacherBatchExamViewModels = GetTeacherBatchExamByTeacherBatchID(3)
                }};

            return teacherBatch.ToList();
        }

        private TeacherTermViewModel GetTeacherTermSingleSecond()
        {

            var schoolservice = new FakeSchoolService();
            return new TeacherTermViewModel
            {
                TeacherTermID = 2, 
                Class = schoolservice.GetClassViewModel(92),// 9B
                IsSupervisor = false,
                Teacher = new FakeTeacherService().GetTeacherByID(1),
                Term = new FakeSchoolService().GetTermViewModel(1)
            };
        }
        private List<TeacherBatchExamViewModel> GetTeacherBatchExamByTeacherBatchID(int techerBatchID)
        {
            return
                new FakeExamService().TeacherBatchExams.Where(id => id.TeacherBatch.TeacherBatchID == techerBatchID)
                    .ToList();
        }

        public TeacherBatchViewModel GeTeacherBatchByID(int teacherbatchID)
        {
            return GeAllTeacherBatch().FirstOrDefault(a => a.TeacherBatchID == teacherbatchID);
        }
        public List<TeacherBatchViewModel> GeTeacherBatchByTeacherID(int teacherID)
        {
            return GeAllTeacherBatch(); // Just for the time being : //
                // GeAllTeacherBatch().Where(a => a.TeacherTerm.Teacher.TeacherID == teacherID).ToList();
        }
        #endregion

    }
}