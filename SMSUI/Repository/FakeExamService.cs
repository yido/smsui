﻿using System.Collections.Generic;
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Exam;
using SMSUI.ViewModels.Staff;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.Repository
{
    public class FakeExamService
    {

        private readonly List<ExamResultViewModel> _examResultViewModels;
        public FakeExamService()
        {
            _examResultViewModels = new List<ExamResultViewModel>();
            Initilize();
        }

        private void Initilize()
        {
         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 1,
             DateOfEntry = DateTime.Now,
             Point = 4
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 1,
             DateOfEntry = DateTime.Now,
             Point = 3
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 1,
             DateOfEntry = DateTime.Now,
             Point = 5
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 1,
             DateOfEntry = DateTime.Now,
             Point = 3
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 1,
             DateOfEntry = DateTime.Now,
             Point = 10
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 1,
             DateOfEntry = DateTime.Now,
             Point = 7
         }); 
        //~second Exam relsut~//
         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 2,
             DateOfEntry = DateTime.Now,
             Point = 0
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 2,
             DateOfEntry = DateTime.Now,
             Point = 5
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 2,
             DateOfEntry = DateTime.Now,
             Point = 1
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 2,
             DateOfEntry = DateTime.Now,
             Point = 2
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 2,
             DateOfEntry = DateTime.Now,
             Point = 6
         });

         _examResultViewModels.Add(new ExamResultViewModel
         {
             ExamResultID = 2,
             DateOfEntry = DateTime.Now,
             Point = 8
         }); 
        }

        readonly FakeSchoolService _schoolService = new FakeSchoolService();

        public IList<ParentExamTypeViewModel> ParentExamTypes { get { return new List<ParentExamTypeViewModel>() { new ParentExamTypeViewModel { ParentExamTypeID = 1, Name = "Standard" }, new ParentExamTypeViewModel { ParentExamTypeID = 2, Name = "Non-Standard" } }; } }
        public IList<ExamTypeViewModel> ExamTypes{
            get
            {
                return new List<ExamTypeViewModel>()
                {
                    
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 1,
                        Name = "Quiz",
                        ParentExamType = GetParentExamType(2)
                    },
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 2,
                        Name = "Test",
                        ParentExamType = GetParentExamType(2)
                    },
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 3,
                        Name = "Assignment",
                        ParentExamType = GetParentExamType(2)
                    },
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 4,
                        Name = "Project",
                        ParentExamType = GetParentExamType(2)
                    },
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 5,
                        Name = "Other",
                        ParentExamType = GetParentExamType(2)
                    },
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 6,
                        Name = "Mid",
                        ParentExamType = GetParentExamType(1)
                    },
                    new ExamTypeViewModel
                    {
                        ExamTypeID = 7,
                        Name = "Final",
                        ParentExamType = GetParentExamType(1)
                    }
                };
            }
        }
        public IList<ExamViewModel> Exams
        {
            get
            {
                return new List<ExamViewModel>{
            new ExamViewModel{ExamID = 1,Name = "Quiz 1",ExamType = GetExamType(1), Mark = 5},
            new ExamViewModel{ExamID = 2,Name = "Quiz 2",ExamType = GetExamType(1), Mark = 5},
            new ExamViewModel{ExamID = 3,Name = "Quiz 3",ExamType = GetExamType(1), Mark = 5},
            new ExamViewModel{ExamID = 4,Name = "Test 1",ExamType = GetExamType(1), Mark = 10},
            new ExamViewModel{ExamID = 5,Name = "Test 2",ExamType = GetExamType(1), Mark = 10},
            new ExamViewModel{ExamID = 6,Name = "Test 3",ExamType = GetExamType(1), Mark = 20},
            new ExamViewModel{ExamID = 7,Name = "Assignment",ExamType = GetExamType(1), Mark = 10},
            new ExamViewModel{ExamID = 8,Name = "Semester Project",ExamType = GetExamType(1), Mark = 15},
            new ExamViewModel{ExamID = 9,Name = "Others",ExamType = GetExamType(1), Mark = 5}};
            }
        }
        public IList<TeacherBatchExamViewModel> TeacherBatchExams
        {
            get
            {
                return new List<TeacherBatchExamViewModel>
                {
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 1, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =1 }, Exam = GetExam(1), IsOwner = true},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 2, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =1 }, Exam = GetExam(2), IsOwner = true},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 3, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =3 }, Exam = GetExam(3), IsOwner = true},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 4, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =1 }, Exam = GetExam(4), IsOwner = true},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 5, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =1 }, Exam = GetExam(4), IsOwner = false},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 6, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =1 }, Exam = GetExam(3), IsOwner = false},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 7, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =3 },Exam = GetExam(2), IsOwner = true},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 8, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =1 },Exam = GetExam(5), IsOwner = false},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 9, TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =3 },Exam = GetExam(3), IsOwner = false},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 10,TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =2 },Exam = GetExam(1), IsOwner = true},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 11,TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =2 },Exam = GetExam(4), IsOwner = false},
                    new TeacherBatchExamViewModel {TeacherBatchExamID = 12,TeacherBatch= new TeacherBatchViewModel{TeacherBatchID =2 },Exam = GetExam(2), IsOwner = false}
                };
            }
        } 

        public ParentExamTypeViewModel GetParentExamType(int parentExamTypeID)
        {
            return ParentExamTypes.FirstOrDefault(f => f.ParentExamTypeID == parentExamTypeID);
        }

        public ExamTypeViewModel GetExamType(int examTypeID)
        {
            return ExamTypes.FirstOrDefault(f => f.ExamTypeID == examTypeID);
        }

        public ExamViewModel GetExam(int examID)
        {
            return Exams.FirstOrDefault(f => f.ExamID == examID);
        }

        public IList<TeacherBatchExamViewModel> GetExamsByTeacherBatch(int teacherBatchID)
        {

            return TeacherBatchExams.Where(w => w.TeacherBatch.TeacherBatchID == teacherBatchID).ToList();
        }
        
        public IList<TeacherBatchExamViewModel> GetExamsBySubject(int classID, int subjectID)
        {
            return
                TeacherBatchExams.Where(
                    w => w.TeacherBatch.Subject.SubjectId == subjectID && w.TeacherBatch.TeacherTerm.Class.ClassId == classID)
                    .ToList();
        }

        public ExamInfoViewModel GetExamInfo()
        {
            return new ExamInfoViewModel
            {
                Classes = _schoolService.ClassViewModels,
                ExamTypes = ExamTypes
            };
        }

        public List<ExamResultViewModel> GetExamResultsByStudentBatchID(int i)
        {
            return _examResultViewModels.Where(id => id.ExamResultID == i).ToList();

        }


        public ExamSchedulePViewModel GetExamSchedulePViewModel()
        {
            return new ExamSchedulePViewModel
            {
                GradeLevelViewModels = new FakeSchoolService().GetAllGradeLevelViewModels(),
                ExamTypeViewModels = ExamTypes.Where(a=>a.ParentExamType.Name == "Standard").ToList(),
                StandardExamViewModels = new List<StandardExamViewModel>
                {
                    new StandardExamViewModel
                    {
                        ExamID = 10,
                        Name = "Mid Exam",
                        ExamDate = DateTime.Now.AddDays(10),
                        ExamTypeViewModel = GetExamType(6),
                        Marks = 30,
                        SubjectViewModels = new FakeSchoolService().GetSubjectViewModels()
                    },
                    new StandardExamViewModel
                    {
                        ExamID = 11,
                        Name = "Final Exam",
                        ExamDate = DateTime.Now.AddDays(30),
                        ExamTypeViewModel = GetExamType(7),
                        Marks = 50,
                        SubjectViewModels = new FakeSchoolService().GetSubjectViewModels()
                    },
                },
                

            };
        } 
    }
}