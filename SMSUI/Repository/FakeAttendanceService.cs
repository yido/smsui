﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Attendance;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.Repository
{
    public class FakeAttendanceService
    {
        private readonly FakeSchoolService _schoolService = new FakeSchoolService();
        private readonly FakeAdmissioinService _admissioinService = new FakeAdmissioinService();

        public IList<ClassViewModel> GetClassesByGradeLevel(int gradeLevelID)
        {
            return _schoolService.ClassViewModels.ToList();
        }

        public IList<StudentBathViewModel> GetStudentsByClass(int classID)
        {
            return _admissioinService.StudentBathViewModels;
        }
        
        public AttendanceInfoViewModel GetAttendanceInfoViewModel()
        {
            return new AttendanceInfoViewModel()
            {
                GradeLevels = _schoolService.GetAllGradeLevelViewModels(),
                Reasons =
                    new List<AbsentReason>
                    {
                        new AbsentReason() {AbsentReasonID = 1, Name = "Health Problem"},
                        new AbsentReason {AbsentReasonID = 2, Name = "Condelence"},
                        new AbsentReason() {AbsentReasonID = 3, Name = "Others"}
                    }

            };
        }
    }
}