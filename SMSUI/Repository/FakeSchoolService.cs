﻿using System;
using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.Repository
{
    public class FakeSchoolService
    {
        public List<AddressViewModel> SchoolAddressViewModels;
        public List<AcademicYearViewModel> AcademicYearViewModels = new List<AcademicYearViewModel>();
        public List<TermViewModel> TermViewModels = new List<TermViewModel>();
        public List<SubjectViewModel> SubjectViewModels = new List<SubjectViewModel>();
        public List<GradeLevelViewModel> GradeLevelViewModels = new List<GradeLevelViewModel>();
        public List<SubjectTypeViewModel> SubjectTypeViewModels = new List<SubjectTypeViewModel>();
        public List<ClassViewModel> ClassViewModels = new List<ClassViewModel>(); 
        public List<SubjectGradeLevelViewModel> SubjectGradeLevelViewModels = new List<SubjectGradeLevelViewModel>(); 

        private readonly FakeAdmissioinService _admissioinService = new FakeAdmissioinService();

        public ClassViewModel GetClassViewModel(int classID)
        {
            return ClassViewModels.FirstOrDefault(a => a.ClassId == classID);
        }
        public FakeSchoolService()
        {
            SchoolAddressViewModels = SchoolAddressViewModels = new List<AddressViewModel>();
            SubjectTypeViewModels.Add(new SubjectTypeViewModel { SubjectTypeId = 1, Name = "Natural Science" }); 
            SubjectTypeViewModels.Add(new SubjectTypeViewModel {SubjectTypeId = 2,Name = "Social Science" });

            ClassViewModels.Add(new ClassViewModel { ClassId = 91, Name = "9-A", IsActive = true, CurrentNoOfStudents = 46, MaxNumberOfStudents = 65,GradeLevelViewModel = GetGradeLevelViewModel(2)});
            ClassViewModels.Add(new ClassViewModel { ClassId = 92, Name = "9-B", IsActive = true, CurrentNoOfStudents = 20, MaxNumberOfStudents = 80, GradeLevelViewModel = GetGradeLevelViewModel(2) });
            ClassViewModels.Add(new ClassViewModel { ClassId = 93, Name = "9-C", IsActive = true, CurrentNoOfStudents = 45, MaxNumberOfStudents = 50, GradeLevelViewModel = GetGradeLevelViewModel(2) });
            ClassViewModels.Add(new ClassViewModel { ClassId = 101, Name = "10-A", IsActive = true, CurrentNoOfStudents = 16, MaxNumberOfStudents = 65, GradeLevelViewModel = GetGradeLevelViewModel(3) });
            ClassViewModels.Add(new ClassViewModel { ClassId = 102, Name = "10-B", IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80, GradeLevelViewModel = GetGradeLevelViewModel(3) });
            ClassViewModels.Add(new ClassViewModel { ClassId = 103, Name = "10-C", IsActive = true, CurrentNoOfStudents = 15, MaxNumberOfStudents = 50, GradeLevelViewModel = GetGradeLevelViewModel(3) });
            ClassViewModels.Add(new ClassViewModel { ClassId = 91, Name = "9-A", IsActive = true, CurrentNoOfStudents = 46, MaxNumberOfStudents = 65 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 92, Name = "9-B",  IsActive = true, CurrentNoOfStudents = 20, MaxNumberOfStudents = 80 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 93, Name = "9-C", IsActive = true, CurrentNoOfStudents = 45, MaxNumberOfStudents = 50 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 101, Name = "10-A",  IsActive = true, CurrentNoOfStudents = 16, MaxNumberOfStudents = 65 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 102, Name = "10-B",  IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 103, Name = "10-C", IsActive = true, CurrentNoOfStudents = 15, MaxNumberOfStudents = 50 });

            ClassViewModels.Add(new ClassViewModel { ClassId = 111, Name = "11-A", IsActive = true, CurrentNoOfStudents = 16, MaxNumberOfStudents = 65 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 112, Name = "11-B", IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80 });
            ClassViewModels.Add(new ClassViewModel { ClassId = 113, Name = "11-C", IsActive = true, CurrentNoOfStudents = 15, MaxNumberOfStudents = 50 });
          
            
            SchoolAddressViewModels.Add(new AddressViewModel
            {
                AddressId = 1,
                Country = "Ethiopia",
                IdentityNumber = "001-07",
                City = "Addis Ababa",
                SubCity = "Yeka",
                Kebele = "03/04",
                StreetName = "Angola St.",
                OfficePhone = "+251-113-142-833",
                MobilePhone = "0913585396",
                Email = "yidogeb@gmail.com",
                IsActive = true,
            });
            SchoolAddressViewModels.Add(new AddressViewModel
            {
                AddressId = 2,
                IdentityNumber = "002-05",
                City = "Addis Ababa",
                SubCity = "Bole",
                Kebele = "05",
                StreetName = "Mandela St.",
                OfficePhone = "+251-113-142-833",
                MobilePhone = "0912127324",
                Email = "Jonas@gmail.com",
                IsActive = true,
            });
        }
        public InitializeSchoolViewModel GetSchoolInitializationViewModel()
        {
            return new InitializeSchoolViewModel
            {
                SchoolInformationViewModel = GetSchoolInformationViewModel(),
                AcademicYearInformationViewModel = GetAcademicInformationViewModel(),
                SubjectGradeLevelInformationViewModel = GetSubjectGradeLevelInformationViewModel()
            };
        }


        private SubjectGradeLevelInformationViewModel GetSubjectGradeLevelInformationViewModel()
        {
            return new SubjectGradeLevelInformationViewModel
            {
                GradeLevelViewModels = GetAllGradeLevelViewModels(),
                SubjectViewModels = GetSubjectViewModels(),
                SubjectGradeLevelViewModels = GetSubjectGradeLevelViewModels()
            };
        }

        public List<SubjectViewModel> GetSubjectViewModels()
        {
            SubjectViewModels.Add(new SubjectViewModel { IsActive = true, SubjectId = 1, Name = "Physics", SubjectTypeViewModel = SubjectTypeViewModels.FirstOrDefault(id => id.SubjectTypeId == 1) });
            SubjectViewModels.Add(new SubjectViewModel { IsActive = true, SubjectId = 2, Name = "Geography", SubjectTypeViewModel = SubjectTypeViewModels.FirstOrDefault(id => id.SubjectTypeId == 2) });
            SubjectViewModels.Add(new SubjectViewModel { IsActive = true, SubjectId = 3, Name = "Mathmatics", SubjectTypeViewModel = SubjectTypeViewModels.FirstOrDefault(id => id.SubjectTypeId == 1) });
            return SubjectViewModels;
        }

        public SubjectViewModel GetSubjectByID(int subjectID)
        {
            return GetSubjectViewModels().FirstOrDefault(sub => sub.SubjectId == subjectID);
        }

        private List<SubjectGradeLevelViewModel> GetSubjectGradeLevelViewModels()
        {
            return GradeLevelViewModels.Select(gradeLevelViewModel => new SubjectGradeLevelViewModel()
            {
                GradeLevelViewModel = gradeLevelViewModel,
                SubjectViewModels = SubjectViewModels.Where(i => i.SubjectId < gradeLevelViewModel.GradeLevelId).ToList()
            }).ToList();
        }

        public List<GradeLevelViewModel> GetAllGradeLevelViewModels()
        { 
            GradeLevelViewModels.Add(new GradeLevelViewModel
            {
                ClassViewModels = new List<ClassViewModel>
                { 
                    new ClassViewModel { ClassId = 81, Name = "8-A", IsActive = true, CurrentNoOfStudents = 46, MaxNumberOfStudents = 65 },
                    new ClassViewModel { ClassId = 82, Name = "8-B", IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80 },
                    new ClassViewModel { ClassId = 83, Name = "8-C", IsActive = true, CurrentNoOfStudents = 15, MaxNumberOfStudents = 50 }
                },
                StudentLevelTypeViewModel = _admissioinService.GetStudentLevelTypeById(2),
                GradeLevelId = 1,
                IsActive = true,
                Name = "Grade Eight"
            });
            GradeLevelViewModels.Add(new GradeLevelViewModel
            {
                ClassViewModels = new List<ClassViewModel>
                { 
                    new ClassViewModel { ClassId = 91, Name = "9-A", IsActive = true, CurrentNoOfStudents = 46, MaxNumberOfStudents = 65 },
                    new ClassViewModel { ClassId = 92, Name = "9-B", IsActive = true, CurrentNoOfStudents = 20, MaxNumberOfStudents = 80 },
                    new ClassViewModel { ClassId = 93, Name = "9-C", IsActive = true, CurrentNoOfStudents = 45, MaxNumberOfStudents = 50 }
                },
                StudentLevelTypeViewModel = _admissioinService.GetStudentLevelTypeById(3),
                GradeLevelId = 2,
                IsActive = true,
                Name = "Grade Nine"
            });
            GradeLevelViewModels.Add(new GradeLevelViewModel
            {
                ClassViewModels = new List<ClassViewModel>
                { 
                    new ClassViewModel { ClassId = 101, Name = "10-A", IsActive = true, CurrentNoOfStudents = 16, MaxNumberOfStudents = 65 },
                    new ClassViewModel { ClassId = 102, Name = "10-B", IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80 },
                    new ClassViewModel { ClassId = 103, Name = "10-C", IsActive = true, CurrentNoOfStudents = 15, MaxNumberOfStudents = 50 }
                },
                StudentLevelTypeViewModel = _admissioinService.GetStudentLevelTypeById(3),
                GradeLevelId = 3,
                IsActive = true,
                Name = "Grade Ten"
            });
            GradeLevelViewModels.Add(new GradeLevelViewModel
            {
                 ClassViewModels = new List<ClassViewModel>
                { 
                   new ClassViewModel { ClassId = 91, Name = "11-A",  IsActive = true, CurrentNoOfStudents = 46, MaxNumberOfStudents = 65 },
                   new ClassViewModel { ClassId = 92, Name = "11-B",  IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80 },
                   new ClassViewModel { ClassId = 93, Name = "11-C",  IsActive = true, CurrentNoOfStudents = 35, MaxNumberOfStudents = 50 }
                },
                 StudentLevelTypeViewModel = _admissioinService.GetStudentLevelTypeById(4),
                GradeLevelId = 4,
                IsActive = true,
                Name = "Grade Eleven"
            });
            GradeLevelViewModels.Add(new GradeLevelViewModel
            {
                ClassViewModels = new List<ClassViewModel>
                { 
                   new ClassViewModel { ClassId = 121, Name = "12-A", IsActive = true, CurrentNoOfStudents = 60, MaxNumberOfStudents = 65 },
                   new ClassViewModel { ClassId = 122, Name = "12-B", IsActive = true, CurrentNoOfStudents = 50, MaxNumberOfStudents = 80 },
                   new ClassViewModel { ClassId = 123, Name = "12-C", IsActive = true, CurrentNoOfStudents = 17, MaxNumberOfStudents = 50 }
                },
                StudentLevelTypeViewModel = _admissioinService.GetStudentLevelTypeById(4),
                GradeLevelId = 5,
                IsActive = true,
                Name = "Grade Twelve"
            });
            GradeLevelViewModels.Add(new GradeLevelViewModel
            {
                ClassViewModels = new List<ClassViewModel>
                { 
                    new ClassViewModel { ClassId = 81, Name = "6-A",  IsActive = true, CurrentNoOfStudents = 46, MaxNumberOfStudents = 65 },
                    new ClassViewModel { ClassId = 82, Name = "6-B",  IsActive = true, CurrentNoOfStudents = 30, MaxNumberOfStudents = 80 },
                    new ClassViewModel { ClassId = 83, Name = "6-C", IsActive = true, CurrentNoOfStudents = 15, MaxNumberOfStudents = 50 }
                },
                StudentLevelTypeViewModel = _admissioinService.GetStudentLevelTypeById(2),
                GradeLevelId = 6,
                IsActive = true,
                Name = "Grade Six"
            });
            return GradeLevelViewModels;
        }
        public SchoolInformationViewModel GetSchoolInformationViewModel()
        {
            var schoolInformation = new SchoolInformationViewModel()
            {
                SchoolTypeViewModels =
                    new List<SchoolTypeViewModel>
                    {
                        {new SchoolTypeViewModel {SchoolTypeId = 1, Name = "Preparatory School"}},
                        {new SchoolTypeViewModel {SchoolTypeId = 2, Name = "High School"}},
                        {new SchoolTypeViewModel {SchoolTypeId = 3, Name = "Elementary School"}},
                        {new SchoolTypeViewModel {SchoolTypeId = 4, Name = "Kg School"}}
                    },
                OwnerShipTypeViewModels =
                    new List<OwnerShipTypeViewModel>
                    {
                        {new OwnerShipTypeViewModel {Name = "Private", OwnerShipTypeId = 1}},
                        {new OwnerShipTypeViewModel {Name = "Goverment", OwnerShipTypeId = 2}},
                        {new OwnerShipTypeViewModel {Name = "Public", OwnerShipTypeId = 3}},
                        {new OwnerShipTypeViewModel {Name = "Mixed", OwnerShipTypeId = 4}}
                    },
                CalenderTypeViewModels =
                    new List<CalenderTypeViewModel>
                    {
                        {new CalenderTypeViewModel {CalenderTypeId = 1, Name = "Ethipian Calander"}},
                        {new CalenderTypeViewModel {CalenderTypeId = 2, Name = "Gregorian Calander"}}
                    },
                SchoolAddressViewModels = SchoolAddressViewModels,
                SchoolLogoPath = "",
                Description = "Description goes here",
                SchoolName = "New Era General School",
                EstablishmentDate = DateTime.Now.Subtract(new TimeSpan(30 * 365, 0, 0, 0))
            };

            schoolInformation.SchoolTypeID = schoolInformation.SchoolTypeViewModels.LastOrDefault().SchoolTypeId;
            schoolInformation.OwnerShipTypeID = schoolInformation.OwnerShipTypeViewModels.FirstOrDefault().OwnerShipTypeId;
            schoolInformation.CalenderTypeID = schoolInformation.CalenderTypeViewModels.FirstOrDefault().CalenderTypeId;
            return schoolInformation;
        }

        public AddressInformationViewModel GetAddressInformationViewModel(int? addressID)
        {
            if (addressID == null)
            {
                return new AddressInformationViewModel
                {
                    AddressViewModel = SchoolAddressViewModels.FirstOrDefault(),
                    AddressTypeViewModels =
                        new List<AddressTypeViewModel>
                        {
                            {new AddressTypeViewModel {AddressTypeId = 1, Name = "Home"}},
                            {new AddressTypeViewModel {AddressTypeId = 2, Name = "Office"}}
                        }
                };
            }

            return new AddressInformationViewModel
            {
                AddressViewModel = SchoolAddressViewModels.FirstOrDefault(f => f.AddressId == addressID),
                AddressTypeViewModels =
                    new List<AddressTypeViewModel>
                        {
                            {new AddressTypeViewModel {AddressTypeId = 1, Name = "Home"}},
                            {new AddressTypeViewModel {AddressTypeId = 2, Name = "Office"}}
                        }
            };
        }
     

        public AcademicYearInformationViewModel GetAcademicInformationViewModel()
        {
            return new AcademicYearInformationViewModel
            {
                AcademicYearViewModels = GetAcademicYearViewModels(),
                TermViewModels = GetTermViewModels(),
                CurrentAcademicYearViewModel = AcademicYearViewModels.FirstOrDefault(y => y.IsActive)
            };
        }

        private List<TermViewModel> GetTermViewModels()
        {
            TermViewModels.Add(new TermViewModel { TermId = 1, Name = "First Term", StartDate = DateTime.Parse("09/15/2014"), EndDate = DateTime.Parse("11/15/2014"), IsActive = true });
            TermViewModels.Add(new TermViewModel { TermId = 2, Name = "Second Term", StartDate = DateTime.Parse("11/16/2014"), EndDate = DateTime.Parse("01/18/2015"), IsActive = true });
            TermViewModels.Add(new TermViewModel { TermId = 3, Name = "Third Term", StartDate = DateTime.Parse("01/20/2015"), EndDate = DateTime.Parse("03/20/2015"), IsActive = true });
            TermViewModels.Add(new TermViewModel { TermId = 4, Name = "Fourth Term", StartDate = DateTime.Parse("03/21/2015"), EndDate = DateTime.Parse("05/22/2015"), IsActive = true });
            return TermViewModels;
        }

        private List<AcademicYearViewModel> GetAcademicYearViewModels()
        {
            AcademicYearViewModels.Add(new AcademicYearViewModel { AcademicYearId = 1, Name = "2007 E.C (2014/2015 G.C)", StartDate = DateTime.Parse("09/15/2014"), EndDate = DateTime.Parse("07/11/2015"), IsActive = true });
            AcademicYearViewModels.Add( new AcademicYearViewModel { AcademicYearId = 2, Name = "2006 E.C (2013/2014 G.C)",StartDate = DateTime.Parse("09/14/2013"), EndDate = DateTime.Parse("07/16/2014"), IsActive = false });
            return AcademicYearViewModels;
        }

        public AcademicYearViewModel GetAcademicYearViewModel(int? acedemicYearID)
        {
            return acedemicYearID == null
                ? new AcademicYearViewModel()
                : GetAcademicYearViewModels().FirstOrDefault(t => t.AcademicYearId == acedemicYearID);
        }

        public TermViewModel GetTermViewModel(int? termID)
        {
            return termID == null
                ? new TermViewModel()
                : GetTermViewModels().FirstOrDefault(t => t.TermId == termID);
        }

        public GradeLevelViewModel GetGradeLevelViewModel(int? gradeLevelId)
        {
            return gradeLevelId == null
                ? new GradeLevelViewModel()
                : GetAllGradeLevelViewModels().FirstOrDefault(g => g.GradeLevelId == gradeLevelId);
        }

        public SubjectInfoViewModel GetSubjectInfoViewModel(int? subjectId)
        {
            return subjectId == null
                ? new SubjectInfoViewModel()
                {
                    SubjectViewModel = new SubjectViewModel(),
                    SubjectTypes = SubjectTypeViewModels
                }
                : new SubjectInfoViewModel()
                {
                    SubjectViewModel = GetSubjectViewModels().FirstOrDefault(g => g.SubjectId == subjectId),
                    SubjectTypes = SubjectTypeViewModels
                };
        }

        public List<StudentBathViewModel> GetAllStudentBatchByTeacherTerm(/*/~Let's pass this as it is for the time being!~//*/)
        {
            //~ Assume Students on a Class where our Teacher Term was assigned! ~//
            return _admissioinService.StudentBathViewModels.ToList();
        }

        internal StudentBathViewModel GetAllStudentBatchByID(int p)
        {
            return _admissioinService.StudentBathViewModels.FirstOrDefault(id => id.StudentBatchID == p);
        }
    }
}