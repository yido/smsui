﻿using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels;
using SMSUI.ViewModels.SchoolSetting;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.Repository
{
    public class FakeAdmissioinService
    {
        public IList<StudentsForRegistrationViewModel> StudentsForRegistrationViewModels;
        public IList<StudentViewModel> StudentViewModels;
        public IList<StudentBathViewModel> StudentBathViewModels;
        //.. Look Ups ..//
        public IList<StudentLevelTypeViewModel> StudentLevelTypeViewModels;
        public IList<StudentTypeViewModel> StudentTypeViewModels;
        public IList<RegistrationStatusViewModel> RegistrationStatusViewModels;

        //.. Fake Services ..//
        private readonly FakeService _fakeService;
        public FakeAdmissioinService()
        {
            _fakeService = new FakeService();
            AddfakeLookUps();
            AddFakeStudentsForRegistration();
            AddFakeStudents();
            AddFakeBatchStudents();
        }




        public RegistrationViewModel GetRegistrationViewModel()
        {
            return new RegistrationViewModel
            {
                StudentsForRegistrationViewModels = StudentsForRegistrationViewModels,
                RegistrationStatusViewModel = RegistrationStatusViewModels,
                StudentLevelType = StudentLevelTypeViewModels,
                StudentType = StudentTypeViewModels
            };
        }

        public AllStudentsForAdmissionViewModel GetAllStudentsForAdmissionViewModel()
        {
            return new AllStudentsForAdmissionViewModel
            {
                StudentsForRegistrationViewModels = StudentsForRegistrationViewModels.Where(status => status.RegistrationStatusId == 2 || status.RegistrationStatusId == 3).ToList(),
                RegistrationStatusViewModel = RegistrationStatusViewModels,
                StudentLevelType = StudentLevelTypeViewModels,
                StudentType = StudentTypeViewModels
            };
        }
        public AdmissionViewModel GetAdmissionViewModel(int? studentId)
        {
            if (studentId == null)
            {
                return new AdmissionViewModel
                {
                    StudentInformationViewModel = new StudentInformationViewModel
                    {
                        StudentViewModel = new StudentViewModel(),
                        StudentLevelTypesModels = StudentLevelTypeViewModels,
                        StudentTypeViewModels = StudentTypeViewModels
                    },
                    StudentContactInformationViewModel = new StudentContactInformationViewModel(),
                    StudentIdCardInformationViewModel = GetNewStudentIdCardInformation()
                };
            }
            return new AdmissionViewModel
            {
                StudentInformationViewModel = new StudentInformationViewModel
                {
                    StudentViewModel = StudentViewModels.FirstOrDefault(id => id.StudentId == studentId),
                    StudentLevelTypesModels = StudentLevelTypeViewModels,
                    StudentTypeViewModels = StudentTypeViewModels
                },
                StudentContactInformationViewModel = GetStudentContactInformation(),
                StudentIdCardInformationViewModel = GetStudentIdCardInformation((int)studentId)
            };
        }
        public FinilazeAdmissionViewModel GetFinilazeAdmissionViewModel(int studentId)
        {
            var _schoolService = new FakeSchoolService();
            var studentViewModel = StudentViewModels.FirstOrDefault(id => id.StudentId == studentId);
            var possibleGradeLevels =
                _schoolService.GetAllGradeLevelViewModels()
                    .Where(
                        id =>
                            studentViewModel != null &&
                            id.StudentLevelTypeViewModel.StudentLevelTypeId == studentViewModel.StudentLevelTypeId)
                    .ToList();

            return new FinilazeAdmissionViewModel
            {
                StudentViewModel = studentViewModel,
                PossibleGradeLevelViewModels = possibleGradeLevels,
                RegistrationStatusViewModels = RegistrationStatusViewModels,
                RelatedClassViewModels = possibleGradeLevels.FirstOrDefault().ClassViewModels
            };
        }
        private StudentIdCardInformationViewModel GetStudentIdCardInformation(int studentId)
        {
            var student = StudentViewModels.FirstOrDefault(id => id.StudentId == studentId);
            var registrationStatusViewModel = RegistrationStatusViewModels.FirstOrDefault(id => id.RegistrationStatusId == student.RegistrationStatusId);
            return new StudentIdCardInformationViewModel
            {
                IdCardViewModel = new IdCardViewModel(),
                RegistrationStatusViewModels = RegistrationStatusViewModels,
                RegistrationStatusViewModel = registrationStatusViewModel,
                IsAdmitted = registrationStatusViewModel != null && registrationStatusViewModel.RegistrationStatusId == 3,
            };
        }
        private StudentIdCardInformationViewModel GetNewStudentIdCardInformation()
        {
            return new StudentIdCardInformationViewModel
            {
                IdCardViewModel = new IdCardViewModel(),
                RegistrationStatusViewModels = RegistrationStatusViewModels,
                RegistrationStatusViewModel = new RegistrationStatusViewModel(),
                IsAdmitted = false
            };
        }

        private StudentContactInformationViewModel GetStudentContactInformation()
        {
            return new StudentContactInformationViewModel
            {
                ContactPersonViewModels = new List<ContactPersonViewModel>(),
                DemographicInfoViewModel = FakeService.GetDemographicInformationUser()
            };
        }


        #region PRIVATE METHODS

        private void AddfakeLookUps()
        {
            StudentLevelTypeViewModels = new List<StudentLevelTypeViewModel>
            {
                new StudentLevelTypeViewModel {StudentLevelTypeId = 1, Name = "Kinder Garten",IsActive = true},
                new StudentLevelTypeViewModel {StudentLevelTypeId = 2, Name = "Elemetary",IsActive = true},
                new StudentLevelTypeViewModel {StudentLevelTypeId = 3, Name = "High School",IsActive = true},
                new StudentLevelTypeViewModel {StudentLevelTypeId = 4, Name = "Preparatory",IsActive = true}
            };
            StudentTypeViewModels = new List<StudentTypeViewModel>
            {
                new StudentTypeViewModel {StudentTypeId = 1, Name = "Regular",IsActive = true},
                new StudentTypeViewModel {StudentTypeId = 2, Name = "Extension",IsActive = true},
                new StudentTypeViewModel {StudentTypeId = 3, Name = "Distance",IsActive = true}
            };
            RegistrationStatusViewModels = new List<RegistrationStatusViewModel>
            {
                new RegistrationStatusViewModel {RegistrationStatusId = 1, Name = "Not Set",IsActive = true},
                new RegistrationStatusViewModel {RegistrationStatusId = 2, Name = "Draft",IsActive = true},
                new RegistrationStatusViewModel {RegistrationStatusId = 3, Name = "Admitted",IsActive = true},
                new RegistrationStatusViewModel {RegistrationStatusId = 4, Name = "Left",IsActive = true}
            };
        }

        private void AddFakeStudentsForRegistration()
        {

            StudentsForRegistrationViewModels = new List<StudentsForRegistrationViewModel>
            {
                new StudentsForRegistrationViewModel
                {
                    StudentId = 1,
                    FirstName = "Yididiya",
                    IdNumber = "0001",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Not Set",
                    RegistrationStatusId = 1
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 2,
                    FirstName = "Yared",
                    IdNumber = "0002",
                    MiddleName = "Dejene",
                    LastName = "Dessalleh",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 3,
                    FirstName = "Yonas",
                    IdNumber = "0003",
                    MiddleName = "Tesfaye",
                    LastName = "Worku",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 4,
                    FirstName = "Baakal",
                    IdNumber = "0004",
                    MiddleName = "Tesfaye",
                    LastName = "Worku",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 5,
                    FirstName = "Tewdoros",
                    IdNumber = "0005",
                    MiddleName = "Wakjira",
                    LastName = "Adhanom",
                    IsActive = false,
                    StudentLevelType = "High School",
                    StudentLevelTypeId = 3,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Not Set",
                    RegistrationStatusId = 1
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 6,
                    FirstName = "Henock",
                    IdNumber = "0006",
                    MiddleName = "Getachew",
                    LastName = "Alemu",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 7,
                    FirstName = "Shalom",
                    IdNumber = "0007",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 8,
                    FirstName = "Hanna",
                    IdNumber = "0008",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Admitted",
                    RegistrationStatusId = 3
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 9,
                    FirstName = "Yishak",
                    IdNumber = "0009",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Admitted",
                    RegistrationStatusId = 3
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 10,
                    FirstName = "Dibora",
                    IdNumber = "0010",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Admitted",
                    RegistrationStatusId = 3
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 11,
                    FirstName = "Lidiya",
                    IdNumber = "0011",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                },
                new StudentsForRegistrationViewModel
                {
                    StudentId = 12,
                    FirstName = "Feven",
                    IdNumber = "0012",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = "Preparatory",
                    StudentLevelTypeId = 4,
                    StudentType = "Regular",
                    StudentTypeId = 1,
                    RegistrationStatus = "Draft",
                    RegistrationStatusId = 2
                }
            };
        }

        private void AddFakeStudents()
        {
            StudentViewModels = new List<StudentViewModel>
            {
                new StudentViewModel
                {
                    StudentId = 1,
                    FirstName = "Yididiya",
                    IdNumber = "0001",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 2,
                    FirstName = "Yared",
                    IdNumber = "0002",
                    MiddleName = "Dejene",
                    LastName = "Dessalleh",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(2),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/Jared.jpg"
                },
                new StudentViewModel
                {
                    StudentId = 3,
                    FirstName = "Yonas",
                    IdNumber = "0003",
                    MiddleName = "Tesfaye",
                    LastName = "Worku",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 4,
                    FirstName = "Alemu",
                    IdNumber = "0004",
                    MiddleName = "Selemon",
                    LastName = "Worku",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(3),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 5,
                    FirstName = "Misganaw",
                    IdNumber = "0005",
                    MiddleName = "Amenu",
                    LastName = "Adhanom",
                    IsActive = false,
                    StudentLevelType = GetStudentLevelTypeById(3),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 6,
                    FirstName = "Kiber",
                    IdNumber = "0006",
                    MiddleName = "Getachew",
                    LastName = "Alemu",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 7,
                    FirstName = "Shalom",
                    IdNumber = "0007",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 8,
                    FirstName = "Hanna",
                    IdNumber = "0008",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = @"../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 9,
                    FirstName = "Yishak",
                    IdNumber = "0009",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = @"../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 10,
                    FirstName = "Dibora",
                    IdNumber = "0010",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = @"../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 11,
                    FirstName = "Lidiya",
                    IdNumber = "0011",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = @"../Content/img/default-avatar.png"
                },
                new StudentViewModel
                {
                    StudentId = 12,
                    FirstName = "Feven",
                    IdNumber = "0012",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = @"../Content/img/default-avatar.png"
                }
            };
        }
        private void AddFakeBatchStudents()
        {
            StudentBathViewModels = new List<StudentBathViewModel>
            {
               {new StudentBathViewModel
               { StudentBatchID = 1,
                   ClassViewModel = new ClassViewModel(),//_fakeService.ClassViewModels.FirstOrDefault(id => id.ClassId == 81)
                   TermViewModel = new TermViewModel(), //_fakeService.GetTermViewModel(1)
                   StudentViewModel = new StudentViewModel
                {
                    StudentId = 1,
                    FirstName = "Yididiya",
                    IdNumber = "0001",
                    MiddleName = "Gebredingel",
                    LastName = "Berhe",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(1),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/default-avatar.png"
                }
               }}
               ,
               {new StudentBathViewModel
               { StudentBatchID = 2,ClassViewModel = new ClassViewModel(),TermViewModel = new TermViewModel(),
                   StudentViewModel = new StudentViewModel
                {
                    StudentId = 2,
                    FirstName = "Yared",
                    IdNumber = "0002",
                    MiddleName = "Dejene",
                    LastName = "Dessalleh",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(2),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/Jared.jpg"
                }
               }},
               {new StudentBathViewModel
               { StudentBatchID = 3,ClassViewModel = new ClassViewModel(),TermViewModel = new TermViewModel(),
                   StudentViewModel = new StudentViewModel
                {
                    StudentId = 3,
                    FirstName = "Yonas",
                    IdNumber = "0003",
                    MiddleName = "Tesfaye",
                    LastName = "Werku",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(2),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/Jared.jpg"
                }
               }}
,
               {new StudentBathViewModel
               { StudentBatchID = 4,ClassViewModel = new ClassViewModel(),TermViewModel = new TermViewModel(),
                   StudentViewModel = new StudentViewModel
                {
                    StudentId = 4,
                    FirstName = "Kiber",
                    IdNumber = "0004",
                    MiddleName = "Temesgen",
                    LastName = "Bamlaku",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(2),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/Jared.jpg"
                }
               }}
,
               {new StudentBathViewModel
               { StudentBatchID = 5,ClassViewModel = new ClassViewModel(),TermViewModel = new TermViewModel(),
                   StudentViewModel = new StudentViewModel
                {
                    StudentId = 5,
                    FirstName = "Yosh",
                    IdNumber = "0006",
                    MiddleName = "Tilaye",
                    LastName = "Meselegne",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(2),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/Jared.jpg"
                }
               }}
,
               {new StudentBathViewModel
               { StudentBatchID = 6,ClassViewModel = new ClassViewModel(),TermViewModel = new TermViewModel(),
                   StudentViewModel = new StudentViewModel
                {
                    StudentId = 6,
                    FirstName = "Dessaleghe",
                    IdNumber = "0002",
                    MiddleName = "Tebarek",
                    LastName = "Bamlaku",
                    IsActive = true,
                    StudentLevelType = GetStudentLevelTypeById(4),
                    StudentType = GetStudentTypeById(1),
                    RegistrationStatus = GetRegistrationStatusById(2),
                    ContactPersonViewModels = GetSampleContactPersons(),
                    AddressViewModel = FakeService.GetAddress(),
                    DemographicInfo = FakeService.GetDemographicInformation(),
                    PhotoPath = "../Content/img/Jared.jpg"
                }
               }}

            };
        }
        private IList<ContactPersonViewModel> GetSampleContactPersons()
        {
            return new List<ContactPersonViewModel>
            {
                new ContactPersonViewModel
                {
                    ContactPersonId = 1,
                    Sex = "Male",
                    FirstName = "Solomon",
                    MiddleName = "Alemu",
                    LastName = "Worku",
                    IsActive = true,
                    Relationship = "Supervisor",
                    IsParent = false,
                    JobTitle = "Senior Software Engineer",
                    AddressViewModel = FakeService.GetContactPersonAddress()
                },
                new ContactPersonViewModel
                {
                    ContactPersonId = 2,
                    Sex = "Female",
                    FirstName = "Selam",
                    MiddleName = "Gebeyehu",
                    LastName = "Sleshi",
                    IsActive = true,
                    Relationship = "Manager",
                    IsParent = false,
                    JobTitle = "MIS Team Manager",
                    AddressViewModel = FakeService.GetContactPersonAddress()
                }
            };
        }

        private RegistrationStatusViewModel GetRegistrationStatusById(int id)
        {
            return RegistrationStatusViewModels.FirstOrDefault(sl => sl.RegistrationStatusId == id);
        }

        private StudentTypeViewModel GetStudentTypeById(int id)
        {
            return StudentTypeViewModels.FirstOrDefault(sl => sl.StudentTypeId == id);
        }

        public StudentLevelTypeViewModel GetStudentLevelTypeById(int id)
        {
            return StudentLevelTypeViewModels.FirstOrDefault(sl => sl.StudentLevelTypeId == id);
        }
    }

        #endregion

}