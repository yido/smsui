﻿using System;
using System.Collections.Generic;
using System.Linq;
using SMSUI.ViewModels.Teacher;
using SMSUI.ViewModels.Teacher.GradeReport;

namespace SMSUI.Repository
{
    public class FakeMyStudentService
    {
       private readonly FakeSchoolService _fakeSchoolService = new FakeSchoolService();
       private readonly FakeTeacherService _fakeTeacherService = new FakeTeacherService();
       private readonly FakeExamService _examService =new FakeExamService();
        public MyStudentsViewModel GetMyStudentsViewModel()
        {
            return new MyStudentsViewModel
            {
                StudentsExamInfoViewModel = GetStudentsExamInfo(),
                ExamResultViewModels = GetStudentsExamResultBySubjectAndClassID(1),
                StudentExamResultInformationViewModel = GetStudentsResultInfo()
            };
        }

        private StudentExamResultInformationViewModel GetStudentsResultInfo()
        {
            return new StudentExamResultInformationViewModel
            {
                ClassWithSubjectAggrigatedViewModel =
                    new List<ClassWithSubjectViewModel>
                    {
                        new ClassWithSubjectViewModel
                        {
                            ClassWithSubjectsAggrigated =
                                new List<string> {"9A - Physics", "9C -Physics", "11-01 -Math", "11-02 -Math"}
                        }
                    },
                ExamsTakenWithnAclass = _examService.GetExamsByTeacherBatch(1).Select(e => e.Exam).ToList(),
                StudentExamResults = GetStudentsExamResult()

            };
        }

        private List<StudentExamResultViewModel> GetStudentsExamResult()
        {
            return new List<StudentExamResultViewModel>
            {
                new StudentExamResultViewModel{StudentBatchViewModel = _fakeSchoolService.GetAllStudentBatchByID(1),ExamResults = _examService.GetExamResultsByStudentBatchID(1) },
                new StudentExamResultViewModel{StudentBatchViewModel = _fakeSchoolService.GetAllStudentBatchByID(1),ExamResults = _examService.GetExamResultsByStudentBatchID(2) }
            };
        }

        private StudentsExamInfoViewModel GetStudentsExamInfo()
        {
            return new StudentsExamInfoViewModel
            {
                StudentBathViewModels = _fakeSchoolService.GetAllStudentBatchByTeacherTerm(),
                TeacherBatchViewModels = _fakeTeacherService.GeTeacherBatchByTeacherID(1) 
            };
        }

        private List<ExamResultViewModel> GetStudentsExamResultBySubjectAndClassID(int classID)
        {
            return new List<ExamResultViewModel>
            {
                {new ExamResultViewModel{ExamResultID = 1,TeacherBatchExamViewModel = _examService.TeacherBatchExams.FirstOrDefault(),Point = 4,DateOfEntry = DateTime.Now}},
                {new ExamResultViewModel{ExamResultID = 2,TeacherBatchExamViewModel = _examService.TeacherBatchExams.LastOrDefault(),Point = 2,DateOfEntry = DateTime.Now}},
                {new ExamResultViewModel{ExamResultID = 3,TeacherBatchExamViewModel = _examService.TeacherBatchExams.FirstOrDefault(),Point = 3,DateOfEntry = DateTime.Now}}
     
            };
        }

    }
}