searchVisible = 0;
transparent = true;
$(document).ready(function () {
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    $('#SchoolInitialization').validate_popover({ onsubmit: false, popoverPosition: 'top' });

    /*SCHOOL INFO WIZARD*/
    $('#wizard').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',
        'lastSelector': '.wizard li.last',
        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;

            $width = 100 / $total;

            $display_width = $(document).width();

            if ($display_width < 400 && $total > 3) {
                $width = 50;
            }
            navigation.find('li').css('width', $width + '%');
        },
        onTabClick: function (tab, navigation, index) {
            // Disable the posibility to click on tabs
            return false;
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            var wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $(wizard).find('.btn-next').hide();
                $(wizard).find('.btn-finish').show();
            } else {
                $(wizard).find('.btn-next').show();
                $(wizard).find('.btn-finish').hide();
            }
        },
        onNext: function (tab, navigation, index) {
            var valid = true;
            switch (index) {
                case 1:
                    valid = validateBasicSchoolSettingStepOne();
                    if (valid) {
                        $.ajax({
                            url: 'School/SaveBasicSchoolInformation',
                            data: JSON.stringify({ schoolInformation: serializeSchoolSettingStepOne() }),
                            contentType: 'application/json',
                            type: 'POST',
                            success: function(response) {
                                //alert(response.message);
                            },
                            error: function (xhr, errorType, exception) {
                                var errorMessage = exception || xhr.statusText || xhr.responseText || errorType;
                                alert(errorMessage);
                            }
                        });
                    }
                case 3:
                    //var table = $('#subjectGradeLevelTable');
                    //alert(table);
                    //var data = table.fnGetData();
                    //alert(data);
                    //console.log(data);
                    break;
                default:
                    break;
            }
            return valid;
        },
        onLast: function (tab, navigation, index) {
            alert('Last Finish!');
        }
    });


    /*ADMISSON PROCESS WIZARD*/
    $('#admissionwizard').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',
        'lastSelector': '.wizard li.last',
        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;

            $width = 100 / $total;

            $display_width = $(document).width();

            if ($display_width < 400 && $total > 3) {
                $width = 50;
            }
            navigation.find('li').css('width', $width + '%');
        },
        onTabClick: function (tab, navigation, index) {
            // Disable the posibility to click on tabs
            return false;
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            var wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $(wizard).find('.btn-next').hide();
                $(wizard).find('.btn-finish').show();
            } else {
                $(wizard).find('.btn-next').show();
                $(wizard).find('.btn-finish').hide();
            }
        },
        onNext: function (tab, navigation, index) {
         
        },
        onLast: function (tab, navigation, index) {
            alert('Last Finish!');
        }
    });

    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function (event) {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $height = $(document).height();
    $('.set-full-height').css('height', $height);

});


//---------SMS SCHOOL INITILIZATION LOGIC HERE ----------//

function validateFields(fieldId, fieldName) {

    var $name = $('#' + fieldId).val();
    var retValue = {};

    if ($name.length == 0) {
        retValue.status = false;

        $.validator.show_error(fieldName + " should not be left empty!", $('#' + fieldId));
        $('#' + fieldId).focus();
    } else {
        retValue.status = true;
        $.validator.hide_validate_popover($('#' + fieldId));
    }

    return retValue.status;
}

function validateBasicSchoolSettingStepOne() {
    var isValid = validateFields("txtschoolname", "School Name") &&
                      validateFields("txtSchoolDescription", "School Description") &&
                      validateFields("txtSchoolEstablishmentDate", "School Establishment Date");
    return isValid;
}
 function serializeSchoolSettingStepOne() {
     var data = {};
     data.SchoolName = $("#txtschoolname").val();
     data.Description = $("#txtSchoolDescription").val();;
     data.EstablishmentDate = $('#txtSchoolEstablishmentDate').val();;
     data.SchoolTypeID = $('#cboSchoolType :selected').val();
     data.OwnerShipTypeID = $('#cboOwnershipType :selected').val();
     data.CalenderTypeID = $('#cboCalendarType :selected').val();

     return data;
 }

//Function to show image before upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        };
        reader.readAsDataURL(input.files[0]);
    }
}













