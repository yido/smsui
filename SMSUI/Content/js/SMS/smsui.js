﻿
/* SINGLE MODAL PURPOSE! */
$(function () {

    $.ajaxSetup({ cache: false });

    $("a[data-modal]").on("click", function (e) {
        // hide dropdown if any
        $(e.target).closest('.btn-group').children('.dropdown-toggle').dropdown('toggle');

        $('#smsModalContent').load(this.href, function () {

            $('#smsModal').modal({
                /*backdrop: 'static',*/
                keyboard: true
            }, 'show');

            bindForm(this);
        });

        return false;
    });


});

function bindForm(dialog) {

    $('form', dialog).submit(function () {
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (result) {
                if (result.success) {
                    if (result.requestFromAddressModal) {
                        alert(result.targetdivId);
                         //$("#addressPluginId").load(result.url);
                         //$('#'+result.targetdivId).load(result.url);
                    }
                    $('#smsModal').modal('hide');

                } else {
                    $('#smsModalContent').html(result);
                    bindForm();
                }
            }
        });
        return false;
    });
}


/* MODAL! */
$(document).ready(function () {
    $('.open-form-modal').magnificPopup({
        type: 'ajax',
        midClick: true,
        modal: true
    });
    
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});


//Chosen JQuery
$(document).ready(function() {
    $(".chosen-select").chosen({
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "95%" }
    });
});

//DataTable
$(document).ready(function() {
    $('.dataGrid').dataTable();
});


//DateTime Picker
$(document).ready(function() {
    $('#form_date').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
});
