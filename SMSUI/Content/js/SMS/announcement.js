﻿
//~ INITIALIZE  ~//
$(document).ready(function () {
    
    // Attach Mail Folder click events handler //
    $('#announcementInboxID').click(function () {
        $('#announcementSentID').css('background', 'transparent');
        $('#announcementInboxID').css('background', '#82E6F4');
        InboxMailFolderClicked();
    });
    
    $('#announcementSentID').click(function () {
        SentMailFolderClicked();
    });
    
    // Attach Go Btn //
    $('#btnEmailGroupsGo').click(function() {
        Go();
    });

    $('#composeEmailGrid').DataTable();
});

function InboxMailFolderClicked() { 
 
    $('#divAnnouncementMailGrid').load('Announcement/_Mail/?folderType=Inbox');
};

function SentMailFolderClicked() {
    $('#announcementInboxID').css('background', 'transparent');
    $('#announcementSentID').css('background', '#82E6F4');
    $('#divAnnouncementMailGrid').load('Announcement/_Mail/?folderType=Sent');
}

function Go() {
    
    var counter = 1;

    var values = $('#lkQuickEmailGroups').val();
    $.ajax({
        type: "POST",
        url: "/Announcement/GetRandomUsers/?filter="+values,
        dataType: "json",
        success: function (data) {

           // alert(data.name);
            var announcementListTable = $('#composeEmailGrid').DataTable();
            announcementListTable.clear();
            $.each(data, function (i, value) {
                announcementListTable.row.add([
                       counter,
                       'Yes',
                       value.FirstName,
                       value.MiddleName,
                       value.UserName,
                       value.RoleName,
                       value.Department,
                            ]).draw();
                counter++;
            });

        }, error: function (error) {
            alert('Error'+' '+error);
        }
    });
    //announcementListTable.ajax.url('/Announcement/GetRandomUsers').load();
    //announcementListTable.row.add([
    //        counter + '.1',
    //        counter + '.2',
    //        counter + '.3',
    //        counter + '.4',
    //        counter + '.5'
    //]).draw();

    //counter++;
    //$.ajax({        
    //    url: '/Announcement/GetRandomUsers',
    //    success: function (s) {

    //        alert(s);
    //        console.log(s);
    //        alert(s);
    //        //announcementListTable.fnClearTable();
    //        //for (var i = 0; i < s.length; i++) {
    //        //    announcementListTable.fnAddData([
    //        //        s[i]
    //        //    ]);
    //        //}
    //        //announcementListTable.fnDraw();
    //    },
    //    error: function() {
    //        alert('Jart!');
    //    } 

    //});


    //function parseData(data) {
    //    jQuery.each(data.rows, function (i, r) {
    //        var rowID = oTable.fnGetPosition(jQuery('#' + r.id));
    //        oTable.fnUpdate([r.id, r.name, r.value, r.hidden], rowID, 0);
    //    });
    //}
}
    
    function updateTable() {
        jQuery.ajax({
            async: true,
            dataType: 'json',
            url: '/Announcement/GetRandomUsers',
            success: function (data) {
                alert(data);
                
            },
            error: function (data) {
                alert('Jart!');
            }
        });
    };


    function RefreshTable(tableId, urlData) {

        alert(tableId +' '+ urlData );
        //Retrieve the new data with $.getJSON. You could use it ajax too
        $.getJSON(urlData, null, function (json) {

            alert(json[0]);
            //var table = $(tableId).dataTable();
           // oSettings = table.fnSettings();
            //table.fnClearTable(this);

            //for (var i = 0; i < json.aaData.length; i++) {
            //    table.oApi._fnAddData(json.aaData[i]);
            //}

            //oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            //table.fnDraw();
        });
    };