﻿var ColorList = ['orange', 'aqua', 'red', 'fuchsia', 'purple','blue', 'navy', 'yellow'];


/* initialize draggable elements
 -----------------------------------------------------------------*/
$(".droppable").droppable({
    //tolerance: "fit",
    //greedy: true,
    scope: 'class-schedule',
    drop: function (event, ui) {
        if ($(this).children('.ui-draggable').length == 1) {
            return;
        }
        if (!ui.draggable.hasClass('draggable-copy')) {
            var color = ui.draggable.css('background-color');
            var copy = $("<div></div>")
                .attr("class", "draggable-copy")
                .text(ui.draggable.text())
                .css('background-color', color)
                .css('border-color', color);
            intializeDraggables(copy);
            $(this).append(copy);
        } else {
            $(this).append(ui.draggable);
        }
    }
});

function intializeDraggables(elements) {
    elements.each(function () {
        $(this).draggable({
            scope: 'class-schedule',
            cursor: "move",
            distance: 5,
            helper: 'clone',
            zIndex: 1070,
            handle: 'span',
            revert: 'invalid',
            revertDuration: 500
        });
        if ($(this).hasClass("draggable-copy")) {
            $(this).on('dblclick', function () {
                $(this).remove();
            });
        }
    });
}

//Double Click Draggable to Remove from Droppable
$(".draggable-copy").on('dblclick', function () {
    alert("Remove the Table");
});

//Load Classes
$('#lkClassScheduleGradeLevels').on('change', function () {
    $.ajax({
        type: 'GET',
        url: '/Attendance/GetClassesByGradeLevel/?gradeLevelID=' + $(this).val(),
        dataType: 'json',
        success: function (result) {
            //clear lkAttendanceClasses
            $('#lkClassScheduleClasses').children('option:gt(0)').remove();
            //append Classes
            $.each(result, function (index, item) {
                $('#lkClassScheduleClasses').append($("<option ></option>")
                            .attr("value", item.ClassId)
                            .text(item.Name));
            });


        }

    });

});


//Get subject 
$('#lkClassScheduleClasses').on('change', function () {
    $.ajax({
        type: 'GET',
        url: '/ClassSchedule/GetSchedules/?classID=' + $(this).val(),
        dataType: 'json',
        success: function (result) {
            
            //clear existing subjects
            $('#draggable-subjects').children('div').remove();
            //append Subjects
            $.each(result.Subjects, function (index, item) {
                var color = ColorList[index];
                var div = "<div class='draggable' style='background-color:" + color + "; border-color:" + color + "'>" + item.Name + "</div>";
                $('#draggable-subjects').append(div);
            });
            //clear existing schedules
            $('#scheduleTable').find('div.ui-draggable').remove();
            //appending schedules
            $.each($('#scheduleTable').find('tr:gt(0)'), function(rowIndex, row) {
                $.each($(row).children('td:gt(0)'), function (columnIndex, column) {
                    
                    var item = findFrom3DArray(result.Schedules, rowIndex + 1, columnIndex + 1);
                    if (item != null) {
                        var subjectCss = findColorByText('draggable-subjects', item[0]);
                        var div = "<div class='draggable-copy' style='background-color:" + subjectCss + "; border-color:" + subjectCss + "'>" + item[0] + "</div>";
                        $(column).append(div);
                    }
                });
            });
            intializeDraggables($('#draggable-subjects div.draggable'));
            intializeDraggables($('#scheduleTable div.draggable-copy'));
            //Toggle Visibility of Controls
            $('#btnSaveClassSchedule').show();
            $('#lblClassScheduleValidation').css('color', 'black').text('');
        }

    });

    //$.get('')
});

//Save 
$('#btnSaveClassSchedule').on('click', function() {
    var schedules = serializeScheduleTable();
    var classSchedule = {};
    classSchedule.Schedules = schedules;
    
    $.ajax({
        url: 'ClassSchedule/SaveClassSchedule',
        data: JSON.stringify({ classSchedule: classSchedule }),
        contentType: 'application/json',
        type: 'POST',
        success: function (response) {
            $('#lblClassScheduleValidation').css('color', 'green').text(response.message);
            $('#btnSaveClassSchedule').hide();
        },
        error: function (xhr, errorType, exception) {
            var errorMessage = exception || xhr.statusText || xhr.responseText || errorType;
            $('#lblClassScheduleValidation').css('color', 'red').text(errorMessage.message);
        }
    });
});


function findFrom3DArray(array, x, y) {
    var searchResult = null;
    for (var i = 0; i < array.length; i++) {
        if (array[i][1] == x && array[i][2] == y) {
            searchResult = array[i];
            break;
        }
    }
    return searchResult;
}

function findElementByText(parent, text) {
    return $('#' + parent + " > div:contains(" + text + ")").first();;
}

function findColorByText(parent,text) {
    var element = findElementByText(parent, text);
    return  element.css("background-color");
}

function serializeScheduleTable() {
    var data = [];
    
    $.each($('#scheduleTable').find('tr:gt(0)'), function (rowIndex, row) {
        $.each($(row).children('td:gt(0)'), function (columnIndex, column) {
            var text = $(column).text();
            if (text !== typeof undefined && text !== '') {
                var element = [];
                element[0] = text;
                element[1] = rowIndex + 1;
                element[2] = columnIndex + 1;
                
                data.push(element);
            }
        });
    });
    return data;
}