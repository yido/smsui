﻿
//Get subject 
$('#lkExamClasses').on('change', function () {
    $.ajax({
        type: 'GET',
        url: '/Exam/GetSubjects/?classID=' + $(this).val(),
        dataType: 'json',
        success: function (result) {
            //clear existing subjects
            $('#lkExamSubjects').children('option:gt(0)').remove();
            //append Subjects
            $.each(result, function (index, item) {
                $('#lkExamSubjects').append($("<option ></option>")
                            .attr("value", item.SubjectId)
                            .text(item.Name));
            });

            //Hide Add new Subject Form
            toggleVisibility(false);
        }

    });

    //$.get('')
});

//On Subject Change
$('#lkExamSubjects').on('change', function () {
    //Show AddNewButton
    $('#btnAddNewExam').css("display", "block");

    //Get Exam List
    $('#examsTable').closest('div').show();
    //append Exams
    $('#examsTable').dataTable({
        "bServerSide": true,
        "sAjaxSource": '/Exam/GetExams/?classID=' + $('#lkExamClasses').val() + '&subjectID=' + $('#lkExamSubjects').val(),
        "bProcessing": true,
        "aoColumns": [
            {
                "sName": "ExamID",
                "bSearchable": false,
                "bSortable": false
            },
            { "sName": "Name" },
            { "sName": "ExamType" },
            { "sName": "Mark" }
        ]
    });
});

//On Add New Click
$('#btnAddNewExam').on('click', function () {
    //Show AddNewButton
    $('#addNewExamForm').show();
});

//On Save Exam Save
$('#btnSaveExam').on('click', function () {
    $.ajax({
        url: 'Exam/SaveExam',
        data: JSON.stringify({ exam: serializeExam() }),
        contentType: 'application/json',
        type: 'POST',
        success: function (response) {
            alert(response.message);
        },
        error: function (xhr, errorType, exception) {
            var errorMessage = exception || xhr.statusText || xhr.responseText || errorType;
            alert(errorMessage);
        }
    });
});


function toggleVisibility(show) {
    if (show) {
        $('#addNewExamForm').show();
        $('#btnAddNewExam').css("display", "block");
        $('#examsTable').parent().closest('div').show();
    } else {
        $('#addNewExamForm').hide();
        $('#btnAddNewExam').css("display", "none");
        $('#examsTable').parent().closest('div').hide();
    }
}

function serializeExam() {
    var data = {};
    var examType = {};
    examType.ExamTypeID = $('#lkExamType').val();
    data.Name = $('#txtExamName').val();
    data.Mark = $('#txtExamMark').val();;
    data.ExamType = examType;
    return data;
}


