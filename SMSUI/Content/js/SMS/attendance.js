﻿var attendances = [];

//Get Classes 
$('#lkAttendanceGradeLevels').on('change', function () {
    $.ajax({
        type: 'GET',
        url: '/Attendance/GetClassesByGradeLevel/?gradeLevelID=' + $(this).val(),
        dataType: 'json',
        success: function (result) {
            //clear lkAttendanceClasses
            $('#lkAttendanceClasses').children('option:gt(0)').remove();
            //append Classes
            $.each(result, function (index, item) {
                $('#lkAttendanceClasses').append($("<option ></option>")
                            .attr("value", item.ClassId)
                            .text(item.Name));
            });
            
            
        }

    });

});

//get Students
$('#lkAttendanceClasses').on('change', function () {
    $.ajax({
        type: 'GET',
        url: '/Attendance/GetStudentsByClass/?classID=' + $(this).val(),
        dataType: 'json',
        success: function (result) {
            //clear lkAttendanceStudents
            $('#lkAttendanceStudents').children('option:gt(0)').remove();
            //append Studens
            $.each(result, function (index, item) {
                $('#lkAttendanceStudents').append($("<option ></option>")
                            .attr("value", item.StudentBatchID)
                            .text(item.StudentViewModel.FirstName + ' ' + item.StudentViewModel.MiddleName + ' ' + item.StudentViewModel.LastName));                
            });
            $(".chosen-select-studens_attendance").chosen({
                '.chosen-select-studens_attendance': {},
                '.chosen-select-studens_attendance-deselect': { allow_single_deselect: true },
                '.chosen-select-studens_attendance-no-single': { disable_search_threshold: 10 },
                '.chosen-select-studens_attendance-no-results': { no_results_text: 'Oops, nothing found!' },
                '.chosen-select-studens_attendance-width': { width: "95%" }
            });
            //Clear Table
            attendances = [];
            $("#tblAbsentsList").find("tr:gt(0)").remove();
        }

    });

});

//get Students
$('#lkAttendanceStudents').on('change', function () {
    var selectedStudents = $('#lkAttendanceStudents').val();
    if (selectedStudents !== null && selectedStudents.length > attendances.length) {
        var student = { StudentBatchID: $('#lkAttendanceStudents').val()[selectedStudents.length - 1], ReasonID: 1, Date: $('#txtAttendaceDate').val() };
        attendances.push(student);
        AppendRowtoAbsentListTable(student);

    }
});

//On Save Attendance
$('#btnSaveAttendance').on('click', function () {
    if (attendances.length > 0) {
        $.ajax({
            url: 'Attendance/SaveAttendance',
            data: JSON.stringify({ attendances: attendances }),
            contentType: 'application/json',
            type: 'POST',
            success: function(response) {
                alert(response.message);
            },
            error: function(xhr, errorType, exception) {
                var errorMessage = exception || xhr.statusText || xhr.responseText || errorType;
                alert(errorMessage);
            }
        });
    }
});



function AppendRowtoAbsentListTable(student) {
    var row = $("<tr />");
    $('#tblAbsentsList').append(row);
    row.append($("<td>" + student.StudentBatchID + "</td>"));
    row.append($("<td>" + student.ReasonID + "</td>"));
    row.append($("<td>" + student.Date + "</td>"));
    row.append($("<td> <span class='btn btn-primary btn-xs'><i class='fa fa-pencil'></i></span></td>"));
}


