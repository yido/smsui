﻿//~ INITIALIZE  ~//
$(document).ready(function() {
    // Attach click handler to the wizard submit button:
    $('#btnSaveAdmissionProcess').click(function() {
        $('#admissionProcessForm').submit();
    });
});

//~  ADMISSION PROCESS ~//
// Handle the form submit event, and make the Ajax request:
$("#admissionProcessForm").on("submit", function(event) {
    event.preventDefault();
    
    //~ Lets collect all information so as to Save/Edit a student ~//
    //var img = $('#picture-container .picture img').attr('src');;
    //alert(img);
    var img1 = $('#wizardPicturePreview').attr('src');
    alert(img1);


    var studentId = $("#admissionProcessStudentId").val();
    var firstname = $("#txtstudentFirstName").val();
    var middlename = $("#txtstudentMiddleName").val();
    var lastname = $("#txtstudentLastName").val();
    var photoPath = '/c/test';
    var isActive = $("#ChkIsActive").val();
    var studentTypeId = $("#lkStudentType").val();
    var studentLevelTypeId = $("#lkStudentLevelType").val();
    var addressId = $("#StudentaddressPluginId").val();
    var demographicInfoId = $("#studentDemographicInfoPlugin").val();
    var registrationStatusId = $("#lkCardStudentRegistrationType").val();
 
    
    alert(firstname + ' ' + middlename);

    var url = $(this).attr("action");
    alert(url);
    var formData = {
        "StudentId": studentId,
        "FirstName": firstname,
        "MiddleName": middlename,
        "LastName": lastname,
        "PhotoPath": photoPath,
        "IsActive": isActive,
        "StudentTypeId": studentTypeId,
        "StudentLevelTypeId": studentLevelTypeId,
        "AddressId": addressId,
        "DemographicInfoId":demographicInfoId,
        "RegistrationStatusId":registrationStatusId
    };
    
    alert(formData);
    
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "ajax",
        success: function(resp) {

            alert('Saved Data!');
            // Do something useful with the data:
            //$("<h3>" + resp.FirstName + " " + resp.LastName + "</h3>").appendTo("#divResult");
        }
    }).done(function (data) {
        alert('process data result!');
        if (data.success === true) {
            alert('Saved Data!');
        } else {
            alert(data.errorMessage);
        }
    }).fail(function (e) {
        alert('Cannot save, error on saving!');
    });;
});

//~FINILAZE ADMISSION~//
$("#lkFinilazeAdmissionStudentGradeLevel").on("onchange",(function(event) {
    event.preventDefault();
    alert('on change of gradelevel');
    var selectedGradeLevelId = $("#IdFinilazeAdmissionStudentGradeLevel").val();
    $.ajax({
        url: "/Admissioin/GetRelatedClassesForGradeLevel",
        data: { "gradelevelId": selectedGradeLevelId },
        type: 'Get',
        success: function (data) {
            alert(data);
            $.each(data, function (i, value) {
                $('#lkFinilazeAdmissionRelatedClass').append($('<option>').text(value).attr('value', value));
            });
        }
    });
}));
 

function onchangeUser(event) {
  
    //alert('on change of gradelevel');
    var selectedGradeLevelId = $("#lkFinilazeAdmissionStudentGradeLevel").val();
  
    $.ajax({
        url: "/Admission/GetRelatedClassesForGradeLevel",
        data: { "gradelevelId": selectedGradeLevelId },
        type: 'Get',
        success: function (data) {
            
            $('#lkFinilazeAdmissionRelatedClass').empty();
           $.each(data, function (i, value) {
               $('#lkFinilazeAdmissionRelatedClass').append($('<option>').text(value.StatusWithName).attr('value', value.ClassId));
            });
        }
    });
}
