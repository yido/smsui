﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMSUI.Repository;
using SMSUI.ViewModels;

namespace SMSUI.Areas.Account.Repositories
{
    public class FakeAccountService
    {
        public List<AccountsViewModel> AllAccountsViewModels;

        public UserAccountViewModel OneUserAccountViewModel;

        public string GetRoleNamByID(int roleId)
        {
            return GetAllRoles().FirstOrDefault(id => id.RoleID == roleId).Name;
        }

        public LoggedInUserViewModel GetLoggedInUsertemp()
        {
            return new FakeService().GetFakeAdminUser();
        }
        public  FakeAccountService()
        {
            AllAccountsViewModels = new List<AccountsViewModel>
            {
                {
                    new AccountsViewModel
                    {
                        UserId = 1,
                        UserName = "yidogeb",
                        FirstName = "Yididiya",
                        MiddleName = "Gebredingel",
                        Lastname = "Berhe",
                        RoleId = 1,
                        RoleName = GetRoleNamByID(1),
                        LastLogin = DateTime.Parse("2014-2-12 00:39:15.523"),
                        IsActive = true,
                        Activity = 23
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 2,
                        UserName = "yareddejne",
                        FirstName = "Yared",
                        MiddleName = "Dejene",
                        Lastname = "Dessaleh",
                        RoleId = 2,
                        RoleName = GetRoleNamByID(2),
                        LastLogin = DateTime.Parse("2014-10-04 20:39:15.523"),
                        IsActive = true,
                        Activity = 80
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 3,
                        UserName = "vuvas54",
                        FirstName = "Yonas",
                        MiddleName = "Tesfaye",
                        Lastname = "Worku",
                        RoleId = 3,
                        RoleName = GetRoleNamByID(3),
                        LastLogin = DateTime.Parse("2014-12-01 20:39:00.111"),
                        IsActive = true,
                        Activity = 91
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 4,
                        UserName = "shal",
                        FirstName = "Shalom",
                        MiddleName = "Gebredingel",
                        Lastname = "Berhe",
                        RoleId = 4,
                        RoleName = GetRoleNamByID(4),
                        LastLogin = DateTime.Parse("2014-09-04 20:39:15.523"),
                        IsActive = true,
                        Activity = 57
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 5,
                        UserName = "lidukiki",
                        FirstName = "Lidiya",
                        MiddleName = "Gebredingel",
                        Lastname = "Berhe",
                        RoleId = 3,
                        RoleName = GetRoleNamByID(3),
                        LastLogin = DateTime.Parse("2014-02-09 20:39:15.523"),
                        IsActive = true,
                        Activity = 8
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 6,
                        UserName = "getted",
                        FirstName = "Tewodros",
                        MiddleName = "Wakjirra",
                        Lastname = "Alemu",
                        RoleId = 2,
                        RoleName = GetRoleNamByID(2),
                        LastLogin = DateTime.Parse("2012-10-04 20:39:15.523"),
                        IsActive = false,
                        Activity = 0
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 7,
                        UserName = "beki",
                        FirstName = "Bereket",
                        MiddleName = "Gebredingel",
                        Lastname = "Berhe",
                        RoleId = 4,
                        RoleName = GetRoleNamByID(4),
                        LastLogin = DateTime.Parse("2014-10-04 20:39:15.523"),
                        IsActive = true,
                        Activity = 79
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 8,
                        UserName = "betse",
                        FirstName = "Betsegaw",
                        MiddleName = "Degfe",
                        Lastname = "Dejenu",
                        RoleId = 3,
                        RoleName = GetRoleNamByID(3),
                        LastLogin = DateTime.Parse("2013-09-04 20:39:15.234"),
                        IsActive = true,
                        Activity = 79
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 9,
                        UserName = "meseret",
                        FirstName = "Befrdu",
                        MiddleName = "Tebarek",
                        Lastname = "Alemayehu",
                        RoleId = 4,
                        RoleName = GetRoleNamByID(4),
                        LastLogin = DateTime.Parse("2012-02-24 20:39:15.523"),
                        IsActive = true,
                        Activity = 33
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 10,
                        UserName = "etetu",
                        FirstName = "Worknesh",
                        MiddleName = "Teshome",
                        Lastname = "Wondmish",
                        RoleId = 3,
                        RoleName = GetRoleNamByID(3),
                        LastLogin = DateTime.Parse("2011-01-01 20:39:15.523"),
                        IsActive = true,
                        Activity = 26
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 11,
                        UserName = "Selam",
                        FirstName = "Selamawit",
                        MiddleName = "Gebrehiwot",
                        Lastname = "Alemu",
                        RoleId = 2,
                        RoleName = GetRoleNamByID(2),
                        LastLogin = DateTime.Parse("2014-02-01 20:39:15.523"),
                        IsActive = true,
                        Activity = 43
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 12,
                        UserName = "getonefish",
                        FirstName = "Fish",
                        MiddleName = "Solomon",
                        Lastname = "Adanu",
                        RoleId = 2,
                        RoleName = GetRoleNamByID(2),
                        LastLogin = DateTime.Parse("2014-11-01 20:39:15.523"),
                        IsActive = true,
                        Activity = 87
                    }
                },
                {
                    new AccountsViewModel
                    {
                        UserId = 13,
                        UserName = "blessed",
                        FirstName = "Yisihak",
                        MiddleName = "Gebredingel",
                        Lastname = "Berhe",
                        RoleId = 3,
                        RoleName = GetRoleNamByID(3),
                        LastLogin = DateTime.Parse("2014-11-04 20:39:15.523"),
                        IsActive = true,
                        Activity = 23
                    }
                },
                  {
                    new AccountsViewModel
                    {
                        UserId = 14,
                        UserName = "HanaBk",
                        FirstName = "Hana",
                        MiddleName = "Bereket",
                        Lastname = "Alemu",
                        RoleId = 3,
                        RoleName = GetRoleNamByID(3),
                        LastLogin = DateTime.Parse("2014-12-04 20:39:15.523"),
                        IsActive = true,
                        Activity = 90
                    }
                }
            };
        }
        public List<AccountsViewModel> GetAllAcccounts()
        {
            return AllAccountsViewModels;
        }

        public void Update(AccountsViewModel accountView)
        {
            var targetedUser = AllAccountsViewModels.FirstOrDefault(id => id.UserId == accountView.UserId);
            if (targetedUser != null)
            {
                targetedUser.FirstName = accountView.FirstName;
                targetedUser.MiddleName = accountView.MiddleName;
                targetedUser.Lastname = accountView.Lastname;
                targetedUser.IsActive = accountView.IsActive;
                targetedUser.RoleId = accountView.RoleId;
                targetedUser.UserName = accountView.UserName;
                targetedUser.LastLogin = DateTime.Now;

            }
        }

        public UserAccountViewModel GetOneUserAccount()
        {
            var fakeuser = new FakeService().GetFakeAdminUser();
            OneUserAccountViewModel = new UserAccountViewModel
            {
                LoggedInUser=fakeuser,
                IsActive = true,
            };

            return OneUserAccountViewModel;
        }

        public List<RoleViewModel> GetAllRoles()
        {
            return new List<RoleViewModel>
            {
                {new RoleViewModel {RoleID = 1, Name = "System Administrator"}},
                {new RoleViewModel {RoleID = 2, Name = "Director"}},
                {new RoleViewModel {RoleID = 3, Name = "Department Head"}},
                {new RoleViewModel {RoleID = 4, Name = "Program Office"}},
                {new RoleViewModel {RoleID = 5, Name = "Teacher"}}
            };


        }

    }
}