﻿using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.Controllers
{
    public class ModalsController : Controller
    {
        public FakeSchoolService SchoolService = new FakeSchoolService();
        //
        // GET: /Modal/

        public ActionResult _AddressModal(int? addressID)
        {
            var addressInformation = SchoolService.GetAddressInformationViewModel(addressID);
            return PartialView(addressInformation);
        }

        [HttpPost]
        public void SaveAddress(AddressViewModel addressViewModel)
        {
            //~ save changes on addressViewModel to SMS DB ~//
            //return Json(new { success = true, url = Url.Action("_AddressPlugin", "School"), requestFromAddressModal = true });
        }

        public ActionResult _AcademicYearModal(int? academicYearID)
        {
            var academicYear = SchoolService.GetAcademicYearViewModel(academicYearID);
            return PartialView(academicYear);
        }

        [HttpPost]
        public void SaveAcademicYear(AcademicYearViewModel academicYearViewModel)
        {
            //Save Academic year
        }

        public ActionResult _TermModal(int? termID)
        {
            var academicYear = SchoolService.GetTermViewModel(termID);
            return PartialView(academicYear);
        }

        [HttpPost]
        public void SaveTerm(TermViewModel termViewModel)
        {
            //Save Term
        }

        public ActionResult _GradeLevelModal(int? gradeLevelID)
        {
            var gradeLevel = SchoolService.GetGradeLevelViewModel(gradeLevelID);
            return PartialView(gradeLevel);
        }

        [HttpPost]
        public void SaveGradeLevel(GradeLevelViewModel gradeLevelViewModel)
        {
            //Save Term
        }

        public ActionResult _SubjectModal(int? subjectID)
        {
            var subject = SchoolService.GetSubjectInfoViewModel(subjectID);
            return PartialView(subject);
        }

        [HttpPost]
        public void SaveSubject(SubjectViewModel subjectViewModel)
        {
            //Save Term
        }
    }
}
