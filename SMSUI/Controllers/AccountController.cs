﻿using System.Linq;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels;

namespace SMSUI.Controllers
{
    public class AccountController : Controller
    {
        
        public FakeAccountService AccountService = new FakeAccountService();

        public ActionResult Index()
        {
            ViewBag.loggedinUser = AccountService.GetLoggedInUsertemp();
            var allAccounts = AccountService.GetAllAcccounts();
            return View(allAccounts);
        }

        public ActionResult _Create(int? userId)
        {
            try
            {
                var roles = AccountService.GetAllRoles();
                var modalData = new AccountEditViewModel
                {
                    Account = new AccountsViewModel(),
                    Roles = roles
                };

                return View(modalData);
            }
            catch
            {
                return View();
            }
        }


        public ActionResult _Edit(int userId)
        {

            try
            {
                var accountService = new FakeAccountService();

                var account = accountService.GetAllAcccounts().FirstOrDefault(id => id.UserId == userId);
                var roles = accountService.GetAllRoles();
                var modalData = new AccountEditViewModel
                {
                    Account = account,
                    Roles = roles
                };

                return View(modalData);
            }
            catch
            {
                return View();
            }
        }


        public ActionResult _Delete(int userId)
        {
            var account = AccountService.GetAllAcccounts().FirstOrDefault(id => id.UserId == userId);
            var roles = AccountService.GetAllRoles();
            var modalData = new AccountEditViewModel
            {
                Account = account,
                Roles = roles
            };
            return View(modalData);
        }
      
    }
}
