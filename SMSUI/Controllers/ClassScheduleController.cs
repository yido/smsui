﻿using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels.ClassSchedule;
using SMSUI.ViewModels.Exam;

namespace SMSUI.Controllers
{
    public class ClassScheduleController : Controller
    {

        private readonly FakeAttendanceService _attendanceService = new FakeAttendanceService();
        private readonly FakeAccountService _accountService = new FakeAccountService();
        private readonly FakeSchoolService _fakeSchoolService = new FakeSchoolService();
        //
        // GET: /ClassSchedule/

        public ActionResult Index()
        {
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            var classScheduleInfo = new ClassScheduleInfoViewModel
            {
                GradeLevels = _fakeSchoolService.GetAllGradeLevelViewModels()
            };
            return View(classScheduleInfo);
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetSchedules(int classID)
        {
            var classSchedule = new ClassSchedule()
            {
                Subjects = _fakeSchoolService.GetSubjectViewModels(),
                Schedules = new string[][]
                {
                    new[] {"Physics", "1", "1"},
                    new[] {"Mathmatics", "2", "1"},
                    new[]  {"Geography", "3", "1"},
                    new[] {"Physics", "1", "5"},
                    new[] {"Mathmatics", "2", "4"},
                    new[]  {"Geography", "3", "3"},
                    new[] {"Physics", "3", "4"},
                    new[] {"Mathmatics", "4", "2"},
                    new[]  {"Geography", "5", "1"},
                }
            };
            return Json(classSchedule, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult SaveClassSchedule(ClassSchedule classSchedule)
        {
            if (classSchedule != null)
            {
                //Todo: Save classSchedule
                return Json(new { success = true, message = "Class Schedule is saved successfully." });
            }
            return Json(new { success = false, message = "Class Schedule is not saved successfully." });
        }

    }
}
