﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels.Attendance;
using SMSUI.ViewModels.Exam;

namespace SMSUI.Controllers
{
    public class AttendanceController : Controller
    {
        private readonly FakeAttendanceService _attendanceService = new FakeAttendanceService();
        private readonly FakeAccountService _accountService = new FakeAccountService();
        private readonly FakeSchoolService _fakeSchoolService = new FakeSchoolService();
        //
        // GET: /Attendance/

        public ActionResult Index()
        {
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            var attendances = _attendanceService.GetAttendanceInfoViewModel();
            return View(attendances);
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetClassesByGradeLevel(int gradeLevelID)
        {
            return Json(_attendanceService.GetClassesByGradeLevel(gradeLevelID), JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetStudentsByClass(int classID)
        {
            return Json(_attendanceService.GetStudentsByClass(classID), JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult SaveAttendance(List<AttendanceViewModel> attendances)
        {
            if (attendances != null && attendances.Count > 0)
            {
                //Todo: Save Attendance
                return Json(new { success = true, message = "Attendance is saved successfully." });
            }
            return Json(new { success = false, message = "Attendance is not saved successfully." });
        }

    }
}
