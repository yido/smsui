﻿using System.Linq;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels.Setting;

namespace SMSUI.Controllers
{
    public class ExamScheduleController : Controller
    {
        //
        // GET: /ExamSchedule/

        public ActionResult Index()
        {
            var model = new FakeExamService().GetExamSchedulePViewModel();
            ViewBag.LoggedInUser =   new FakeAccountService().GetLoggedInUsertemp();

            return View(model);
        }


        public ActionResult PeriodSetting(int? periodId)
        {
            var vm = new FakeService().GetPeriodSetting();
            var pageVM = new PeriodSettingPageViewModel
            {
                PeriodSettingViewModel = vm,
                PeriodViewModels = new FakeService().GetPeriods()
            };
            ViewBag.LoggedInUser = new FakeService().GetFakeAdminUser();
            return View(pageVM);
        }


    }
}
