﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels;
using SMSUI.ViewModels.Announcement;

namespace SMSUI.Controllers
{
    public class AnnouncementController : Controller
    {
        public FakeAccountService AccountService = new FakeAccountService();
        public FakeAnnouncementService AnnouncementService = new FakeAnnouncementService();
        private IntranetAnnouncementInformationViewModel _intranetAnnouncement;
        public ActionResult Index()
        {
            ViewBag.loggedinUser = AccountService.GetLoggedInUsertemp();
            _intranetAnnouncement = AnnouncementService.GetIntranetAnnoucementInfo();
            return View(_intranetAnnouncement);
        }

        //
        // GET: /Announcement/Details/5

        public ActionResult MailDetail(int? userAnnouncementID,string context)
        {
            ViewBag.loggedinUser = AccountService.GetLoggedInUsertemp();
            switch (context)
            {
                case "Inbox":
                    {
                        var mailDetailViewModel = GetFakeCachedMailDetail(userAnnouncementID);
                        return View(mailDetailViewModel);
                    }

                case "Sent":
                {
                    var mailDetailViewModel = GetFakeCachedSentMailDetail(userAnnouncementID);
                    return View(mailDetailViewModel);
                    }
            }
            //~ If We go this far sms is in danger! ;) ~//
            return View();
        }

        private MailDetailViewModel GetFakeCachedMailDetail(int? userAnnouncementID)
        {
            _intranetAnnouncement = AnnouncementService.GetIntranetAnnoucementInfo();
            return new MailDetailViewModel
            {
                AnnouncementViewModel =
                    _intranetAnnouncement.Announcements.FirstOrDefault(id => id.AnnouncementID == userAnnouncementID),
                FakeSetting = "Inbox",
                InBoxMails = _intranetAnnouncement.InBoxMails
            };
        }
        private MailDetailViewModel GetFakeCachedSentMailDetail(int? userAnnouncementID)
        {
            _intranetAnnouncement = AnnouncementService.GetIntranetAnnoucementInfo();
            var sentAnnouncement = AnnouncementService.GetIntranetSentAnnoucementInfo();
            return new MailDetailViewModel
            {
                AnnouncementViewModel =
                    sentAnnouncement.FirstOrDefault(id => id.AnnouncementID == userAnnouncementID),
                FakeSetting = "Sent",
                InBoxMails = _intranetAnnouncement.InBoxMails
            };
        }
        public ActionResult _Mail(string folderType)
        {
            switch (folderType)
            {
                case "Inbox":
                {
                    var announcements = AnnouncementService.GetIntranetAnnoucementInfo().Announcements;
                    return View(announcements);
                }

                case "Sent":
                {
                    var sentAnnouncement = AnnouncementService.GetIntranetSentAnnoucementInfo();
                    return View(sentAnnouncement);
                }
            }
            //~ If We go this far sms is in danger! ;) ~//
            return View();
        }
        //
        // GET: /Announcement/Create

        public ActionResult ComposeMail()
        {
            ViewBag.loggedinUser = AccountService.GetLoggedInUsertemp();
            var composeMailModel = AnnouncementService.GetComposeMailInfo();
            return View(composeMailModel);
        }

        public JsonResult GetRandomUsers(int[] filter)
        {
            List<UserViewModel> users = filter != null
                ? AnnouncementService.GetSampleUsers().Where(id => id.RoleID == filter[0]).ToList()
                : AnnouncementService.GetSampleUsers();
            return Json(users);
        }

        //
        // POST: /Announcement/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Announcement/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Announcement/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Announcement/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Announcement/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
