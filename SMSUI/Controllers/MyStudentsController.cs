﻿using System.Web.Mvc;
using SMSUI.Repository;

namespace SMSUI.Controllers
{
    public class MyStudentsController : Controller
    {
        private readonly FakeMyStudentService _fakeMyStudentService = new FakeMyStudentService();
        private readonly FakeAccountService _accountService = new FakeAccountService();
        public ActionResult Index()
        {
            var myStudentsViewModel = _fakeMyStudentService.GetMyStudentsViewModel();
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(myStudentsViewModel);
        }


        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /MyStudents/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MyStudents/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MyStudents/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /MyStudents/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MyStudents/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /MyStudents/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
