﻿using System.Linq;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels;
using SMSUI.ViewModels.SchoolSetting;

namespace SMSUI.Controllers
{
    public class SchoolController : Controller
    {

        public FakeAccountService AccountService = new FakeAccountService();
        public FakeSchoolService SchoolService = new FakeSchoolService();
        public AddressInformationViewModel AddressInformation;
        public ActionResult Index()
        {
           InitializeSchoolViewModel schoolInitialazeViewModel = SchoolService.GetSchoolInitializationViewModel();
            ViewBag.loggedinUser = AccountService.GetLoggedInUsertemp();
            return View(schoolInitialazeViewModel);
        }

        public ActionResult _BasicSchoolInformation()
        {
            var schoolInfo = SchoolService.GetSchoolInformationViewModel();
            return PartialView(schoolInfo);
        }

        [HttpPost]
        public ActionResult SaveBasicSchoolInformation(SchoolInformationViewModel schoolInformation)
        {
            if (schoolInformation != null)
            {
                //Todo: Save school information
                return Json(new { success = true, message = "School information is saved successfully." });
            }
            return Json(new { success = false, message = "School information is not saved successfully." });
        }

        public ActionResult _AddressPartialView(int? addressId, string targetdivId)
        {
            ViewBag.targetdivId = targetdivId;
             AddressInformation = SchoolService.GetAddressInformationViewModel(addressId);
            if (addressId != null)
                AddressInformation.AddressViewModel =
                    SchoolService.SchoolAddressViewModels.FirstOrDefault(id => id.AddressId == addressId);
            return View(AddressInformation);
        }

        [HttpPost]
        public ActionResult _AddressPartialViewPost(AddressViewModel addressViewModel, string targetdivId)
        {
            //~ save changes on addressViewModel to SMS DB ~//
            return Json(new { success = true, targetdivId, url = Url.Action("_AddressPlugin", "School"), requestFromAddressModal = true }); 
        }

        public ActionResult _AddressPlugin()
        {
            var orginalAddress = SchoolService.SchoolAddressViewModels.FirstOrDefault(); 
            orginalAddress.City = orginalAddress.City == "Addis Ababa" ? "Hawassa" : "Addis Ababa";
            return PartialView(SchoolService.SchoolAddressViewModels);
        }

    }
}
