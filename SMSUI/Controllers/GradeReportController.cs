﻿using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels.Teacher;

namespace SMSUI.Controllers
{
    public class GradeReportController : Controller
    {
        //
        // GET: /GradeReport/

        public ActionResult Index()
        {
            //Here we need to validate to show GO TO Grade Report  if not redirect to submission page
            return RedirectToAction("SubmissionStatus");
        }

        public ActionResult SubmissionStatus()
        {
            var teacherTerm = new FakeTeacherService().GetTeacherTermViewModel(1);//Assume: This is logged in Teacher + IsSupervisor
            ViewBag.loggedinUser = new FakeAccountService().GetLoggedInUsertemp();
            var model = new FakeGradeReportService().GetClassResultStatusByTeacherTerm(teacherTerm);
            
            return View(model);
        }

        public ActionResult GetExamResults(int classId)
        {
            ViewBag.loggedinUser = new FakeAccountService().GetLoggedInUsertemp();
            var model =
                new FakeGradeReportService().GetExamResultforClass(classId);

          return View("ExamResults",model);
        }

        [HttpPost]
        public JsonResult ConfirmExamResult(StudentResultViewModel data)
        {
            return Json(" Exam Result Confirmed");
        }

        public ActionResult GetGradeReport()
        {
            var model = new FakeGradeReportService().GradeReport();
            ViewBag.loggedinUser = new FakeAccountService().GetLoggedInUsertemp();
            return View("GradeReport",model);
        }

        public ActionResult StudentGradeReport(int studentID)
        {
            ViewBag.loggedinUser = new FakeAccountService().GetLoggedInUsertemp();
            var student = new FakeGradeReportService().GetStudentGradeReport(studentID);
            return View(student);

        }

    }
}
