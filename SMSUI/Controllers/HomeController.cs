﻿using System.Web.Security;
using SMSUI.Repository;
using SMSUI.ViewModels;
using System.Web.Mvc;

namespace SMSUI.Controllers
{
    public class HomeController : Controller
    {
        private FakeService _fakeService = new FakeService();
        public ActionResult Index()
        {
            var lggedInUsesr = Session["USER"] as LoggedInUserViewModel;
            if (lggedInUsesr != null)
            {
                return View(lggedInUsesr);
            }
            return RedirectToAction("_LoginPartial", "Home");
        }

        [HttpGet]
        public ActionResult _LoginPartial()
        {
            return View();
        }

        [HttpPost]
        public ActionResult _Login(LoginViewModel userViewModel, string returnUrl)
        {
            var user = new FakeService().GetFakeAdminUser();
            Session["USER"] = user;

            FormsAuthentication.SetAuthCookie(user.UserName, true);

            return RedirectToAction("Index","Home");
        }

        public ActionResult _LockScreen()
        {
            RedirectToAction("Index", "Home");
            return View();
        }
        public ActionResult MyProfile()
        {
           return RedirectToAction("Index", "Profile");
        }


        public ActionResult _ContactPersonPlugin()
        {
            var contactPersons = _fakeService.ContactPersonViewModels;
            return View(contactPersons);
        }
        public ActionResult _ContactPersonModalView(int? contactPersonId)
        {
            var contactPersons = _fakeService.ContactPersonViewModels.Find(id => id.ContactPersonId == contactPersonId);
            return View(contactPersons);
        }
        [HttpPost]
        public ActionResult _ContactPersonModalViewPost(ContactPersonViewModel contactPersonViewModel)
        {
            return View();
        }

        public ActionResult DemographicInfoModal(int? demographicInformationID)
        {
            DemographicInfoViewModel defaultDemographicInfo = FakeService.GetDemographicInformationUser();
            return View(defaultDemographicInfo);
        }

        [HttpPost]
        public ActionResult SaveDemographicInfo(DemographicInfoViewModel demographicInfo)
        {
            var defaultDemographicInfo = demographicInfo ?? FakeService.GetDemographicInformationUser();


            return View("DemographicInfoModal",defaultDemographicInfo);
        }
    }

    public static class SMSHelper
    {
        public static LoggedInUserViewModel GetLoggedCurrentInUser (this HtmlHelper helper)
        {
            return  new FakeService().GetFakeAdminUser(); 
        }

    }
}
