﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels.Exam;

namespace SMSUI.Controllers
{
    public class ExamController : Controller
    {
        private readonly FakeAccountService _accountService = new FakeAccountService();
        private readonly FakeSchoolService _fakeSchoolService = new FakeSchoolService();
        readonly FakeExamService _examService = new FakeExamService();
        //
        // GET: /Exam/

        public ActionResult Index()
        {
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(_examService.GetExamInfo());
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetSubjects(int classID)
        {
            return Json(_fakeSchoolService.GetSubjectViewModels(), JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpGet]
        public ActionResult GetExams(int classID, int subjectID)
        {
            var exams = _examService.Exams;
            return Json(new
            {
                 sEcho="1",
                iTotalRecords = 97,
                iTotalDisplayRecords = 3,
                aaData = exams.Select(s => new[]
                {
                    s.ExamID.ToString(), s.Name, s.ExamType.Name, s.Mark.ToString()
                }).ToList()
            }, JsonRequestBehavior.AllowGet);

        }

        [System.Web.Http.HttpPost]
        public JsonResult SaveExam(ExamViewModel exam)
        {
            if (exam != null)
            {
                //Todo: Save Exam
                return Json(new { success = true, message = "Exam is saved successfully." });
            }
            return Json(new { success = false, message = "Exam is not saved successfully." });
        }
    }
}
