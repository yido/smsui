﻿using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels;

namespace SMSUI.Controllers
{
    public class AdmissionController : Controller
    {
        private readonly FakeAccountService _accountService = new FakeAccountService();
        private readonly FakeAdmissioinService _fakeAdmissioinService = new FakeAdmissioinService();
        private readonly FakeSchoolService _schoolService = new FakeSchoolService();


        public ActionResult Index()
        {
            var registrationViewModel = _fakeAdmissioinService.GetRegistrationViewModel();
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(registrationViewModel);
        }

       
        public ActionResult AdmissionProcess(int? studentId)
        {
            var admissionViewModel = _fakeAdmissioinService.GetAdmissionViewModel(studentId);
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(admissionViewModel);
        }
        public ActionResult AllStudentsForAdmissioin()
        {
            var allStudentsForAdmission = _fakeAdmissioinService.GetAllStudentsForAdmissionViewModel();
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(allStudentsForAdmission);
        }
        public ActionResult FinalizeAdmission(int studentId)
        {
            var finilazeAdmissionViewModel = _fakeAdmissioinService.GetFinilazeAdmissionViewModel(studentId);
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(finilazeAdmissionViewModel);
        }
    
        //
        // POST: /Admission/AddNewStudentEntry

        [HttpPost]
        public ActionResult AddNewStudentEntry(StudentViewModel studentViewModel)
        {
            try
            {
                //~ Save To Db~//
                var exampleStu = studentViewModel;
                exampleStu.StudentId = 1000;
                _fakeAdmissioinService.StudentViewModels.Add(exampleStu);
                //~ Save and Update~// 

                return
                    Json(
                        new
                        {
                            success = true,
                            targetdivId = "IDPrint",
                            url = Url.Action("AdmissionProcess", "Admission",new {studentId =1}),
                            requestFromAddressModal = true
                        });
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public JsonResult GetRelatedClassesForGradeLevel(int gradelevelId)
        {
            var relatedClasses =
                _schoolService.GetAllGradeLevelViewModels().Find(id => id.GradeLevelId == gradelevelId).ClassViewModels;
            return Json(relatedClasses, JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Admission/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Admission/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admission/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Admission/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
