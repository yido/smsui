﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMSUI.Repository;
using SMSUI.ViewModels.Staff;

namespace SMSUI.Controllers
{
    public class TeacherController : Controller
    {
        //
        // GET: /Teacher/
        private readonly FakeTeacherService _teacherService = new FakeTeacherService();
        private  readonly FakeAccountService _accountService = new FakeAccountService();
        private IEnumerable<TeacherViewModel> _teachers = new List<TeacherViewModel>(); 
        public ActionResult Index()
        {

            _teachers = _teacherService.GetAllTeachers().ToList();         
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(_teachers);
        }

        public ActionResult _TeacherBatch()
        {
            _teachers = _teacherService.GetAllTeachers();
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            return View(_teachers);
        }

        public ActionResult _UpdateTeacherBatch( int teacherId)
        {
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            var teacherBatch = _teacherService.GeTeacherBatchByTeacherID(teacherId);
            var service = new FakeSchoolService();

            var page = new TeacherBatchPageViewModel
            {
                TeacherBatchViewModels = teacherBatch,
               };


            return View(page);
        }

        [HttpPost]
        public ActionResult _UpdateTeacherBatch(TeacherBatchViewModel teacherBatch)
        {
            var term = new FakeSchoolService().GetTermViewModel(1);//Assuming this is the current term

            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
          return  RedirectToAction("_teacherbatch");
        }


        public ActionResult _Teacher(int? teacherID)
        {
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            var teacher = _teacherService.GetTeacherByID(teacherID);
            return View(teacher);

        }

        [HttpPost]
        public ActionResult _Teacher(TeacherViewModel teacherViewModel)
        {
            ViewBag.loggedinUser = _accountService.GetLoggedInUsertemp();
            var model = new TeacherViewModel
            {
                FirstName = teacherViewModel.FirstName,
                MiddleName = teacherViewModel.MiddleName,
                LastName = teacherViewModel.LastName,
                IdNumber = teacherViewModel.IdNumber,
                TeacherID = 100,
                IsActive = true,
                Address = teacherViewModel.Address ?? FakeService.GetAddress(),
                DemographicInformation = teacherViewModel.DemographicInformation??FakeService.GetDemographicInformationUser()
            };
            return View("_Teacher",model);
        }
    }
}
