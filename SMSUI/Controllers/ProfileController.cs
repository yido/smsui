﻿using System.Linq;
using SMSUI.Repository;
using SMSUI.ViewModels;
using System.Web.Mvc;

namespace SMSUI.Controllers
{
    public class ProfileController : Controller
    {
        
        [HttpGet]
        public ActionResult Index()
        {
            var lggedInUsesr = Session["USER"] as LoggedInUserViewModel;
            if (lggedInUsesr != null)
            {
                return View(lggedInUsesr);
            }
            return RedirectToAction("_LoginPartial", "Home");
        }

        public ActionResult Modal()
        {
            return View();
        }

        [HttpPost]
        public ActionResult _ProfilePartial(LoggedInUserViewModel updatedUserViewModel)
        {

            var lggedInUsesr = Session["USER"] as LoggedInUserViewModel;
            lggedInUsesr.MiddleName = lggedInUsesr.MiddleName == "Yared" ? "Gebredingel" : "Yared";
            Session["USER"] = lggedInUsesr;
            return RedirectToAction("Index", "Test");
        }


        public ActionResult _Create()
        {
            try
            {
               var AccountService = new FakeAccountService();

                var account = AccountService.GetAllAcccounts().FirstOrDefault(id => id.UserId == 1);
                var roles = AccountService.GetAllRoles();
                var modalData = new AccountEditViewModel
                {
                    Account = account,
                    Roles = roles
                };

                return View(modalData);
            }
            catch
            {
                return View();
            }
        }



        public int TestAction()
        {
            return 19;
        }
        //
        // GET: /Test/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Test/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Test/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Test/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Test/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Test/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Test/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
